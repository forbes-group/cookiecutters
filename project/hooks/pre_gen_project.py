from collections import OrderedDict
from glob import glob
import os.path
import sys
from warnings import warn

from cookiecutter.config import get_config


def check_inputs():
    """Check inputs, providing feedback if required fields are not filled."""
    cookiecutter_context = {{cookiecutter | pprint}}
    if os.path.exists(".cookiecutter.yaml"):
        config = get_config(".cookiecutter.yaml")
    elif os.path.exists("../.cookiecutter.yaml"):
        # Needed because we often work from the symlink.
        config = get_config("../.cookiecutter.yaml")
    else:
        warn(
            "No .cookiecutter.yaml file found. "
            + "Not performing pre_gen_project checks."
        )
        return

    okay = True
    for key in config["default_context"]:
        if key not in cookiecutter_context:
            print(f"ERROR: {key=} in `.cookiecutter.yaml` does not exist! (Spelling?)")
            okay = False
    if not "{{ cookiecutter.project_name }}":
        print("ERROR: Empty `project_name`.")
        okay = False
    if not "{{ cookiecutter.one_line_description }}":
        print("ERROR: Empty `one_line_description`.")
        okay = False
    if not okay:
        sys.exit(1)


def add_symlink_for_slug_dir():
    """Add a symlink to the current directory for the project slug.

    Since Cookiecutter can only install into the directory
    `{{cookiecutter.project_slug}}`, we cannot update the "current" directory.
    To get around this, we first make a symlink to the current directory

        ln -s . {{cookiecutter.project_slug}}

    This allows us to install into the current directory.

    To prevent possible issues, we only do this under the following conditions:

    1. We are in an empty directory called {{cookiecutter.project_slug}}.
    2. The parent directory has only dot files.
    """
    slug_dir = "{{cookiecutter.project_slug}}"
    if os.path.islink(slug_dir):
        print(f"Not adding symlink: `{slug_dir}` is already a symlink.")
        return
    os.chdir("..")
    non_dot_files = glob("*")
    if not os.path.exists(slug_dir):
        warn(f"Not adding symlink: missing directory `{slug_dir}`.")
        return
    if not os.path.isdir(slug_dir):
        warn(f"Not adding symlink: `{slug_dir}` is not a directory.")
        return
    if os.listdir(slug_dir):
        warn(f"Not adding symlink: `{slug_dir}` is not empty.")
        return

    non_dot_files.remove(slug_dir)

    if non_dot_files:
        warn(f"Not adding symlink: dir has non-dot files {non_dot_files}")
        return

    os.rmdir(slug_dir)
    os.symlink(".", slug_dir)


if __name__ == "__main__":
    check_inputs()
    add_symlink_for_slug_dir() # Should no longer be needed with new update procedure
