# OrderedDict needed because of {{cookiecutter._symlinks}}
from collections import OrderedDict
from glob import glob
import os.path
import shutil


def rm_rf(path):
    """Forcibly remove both files and directories, like `rm -rf`"""
    if os.path.isdir(path) and not os.path.islink(path):
        shutil.rmtree(path)
    elif os.path.exists(path) or os.path.islink(path):
        os.remove(path)


def make_symlinks():
    """Cookiecutter does not yet support symlinks, so we make them here.

    https://github.com/cookiecutter/cookiecutter/issues/865
    https://github.com/cookiecutter/cookiecutter/issues/1420
    """
    # {% if cookiecutter._symlinks %}
    symlinks = {{cookiecutter._symlinks}}
    for dest in symlinks:
        if os.path.exists(dest):
            rm_rf(dest)
        os.symlink(symlinks[dest], dest)
    # {% endif %}


def delete_unused_features():
    """Delete any files associated with unused features"""
    # {% if cookiecutter.use_manim == "no" %}
    # {% endif %}
    # {% if cookiecutter.use_test_assignments == "no" or cookiecutter.use_course == "no" %}
    for file in glob("src/{{cookiecutter.pkg_name}}/assignment_*.py"):
        rm_rf(file)
    for file in glob("tests/test_assignment_*.py"):
        rm_rf(file)
    for file in glob("tests/test_official_assignment_*.py"):
        rm_rf(file)
    # {% endif %}
    # {% if cookiecutter.use_course == "no" %}
    for file in (
        [
            "InstructorNotes.md",
            "Docs/Syllabus.md",
            "Docs/InstructorNotes.md",
            "Docs/ClassLog.md",
            "Docs/Prerequisites/",
        ]
        + glob("Docs/Assignments*")
        + glob("Docs/_static/wsu*")
    ):
        rm_rf(file)
    # {% endif %}
    # {% if cookiecutter.use_docs == "no" %}
    rm_rf("Docs")
    # {% endif %}

    # {% if cookiecutter.use_conda in ["micromamba", "conda", "no"] %}
    rm_rf("anaconda-project.yaml")
    # {% endif %}
    # {% if cookiecutter.use_conda in ["anaconda-project", "no"] %}
    rm_rf("environment.yaml")
    # {% endif %}

    # {% if cookiecutter.use_pyproject == "no" and cookiecutter.use_src == "no" %}
    rm_rf("pyproject.toml")
    # {% endif %}


if __name__ == "__main__":
    make_symlinks()
    delete_unused_features()
