name: {{ cookiecutter.env_name }}
channels:
  - defaults
dependencies:
  - python>={{ cookiecutter.python_minver }},<{{ cookiecutter.python_maxver }}

  # Required conda dependencies (cookiecutter.conda_dependencies)
{%- for dep in cookiecutter.conda_dependencies %}
  - {{ dep }}
{%- endfor %}

{#------------------------------------------------------#}
{#- Pure conda - no pyproject.toml.  Everything is here #}
{%- if cookiecutter.use_pyproject == "no" %}

{%-   if cookiecutter.use_docs == "yes" %}
  # Documentation
  - pandoc
{%-     if cookiecutter.use_manim == "yes" %}
  - pycairo  # Needed by manim
  - pango    # Needed by manim
  #- manimpango    # Needed by manim
  #- ffmpeg   # Needed by manim
{%-     endif %}    
{%-   endif %}

  # Pip dependencies
  - pip
  - pip:
    - ipykernel
    - mmf-setup
    #- ruamel-yaml

    # Pip dependencies (cookiecutter.pip_dependencies)
{%-   for dep in cookiecutter.pip_dependencies %}
    - {{ dep }}
{%-   endfor %}

{%    if cookiecutter.performance == "yes" %}
    # Performance
    # Use source for now - needed for ARM platforms
    - git+https://github.com/pyFFTW/pyFFTW.git
    - Cython
    - numba
    - numexpr
{%-   endif %}

    # Testing
    - pytest-cov
    - pytest-flake8
    - pytest-html
    - pytest-xdist

{%-   if cookiecutter.use_docs == "yes" %}
    #- sphinx>=7.0.1            # Main documentation system - will be pulled in by others
{%-     if cookiecutter.use_myst_substitutions == "yes" %}
    # We use a custom fork of myst-parser to support our Syllabus.md substitution.
    # https://github.com/executablebooks/MyST-Parser/issues/510
    # We must install myst-nb from pip as well, otherwise it will pull the old version
    # from conda.
    - git+https://github.com/mforbes/MyST-Parser.git@issue_510#egg=myst-parser==0.15.2
{%-     else %}
    #- myst-parser>=2.0.0        # Support for MyST .md files. Provided by myst-nb
{%-     endif %}
    - myst-nb>=0.17.2           # Support for notebooks as MyST .md files.
    - sphinx-design>=0.4.1      # Responsive web design
    - sphinx-book-theme>=1.0.1  # Main documentation theme.
    - sphinx-comments           # Hypothes.is comments and annotations
    - sphinxext-opengraph       # Provides Open Graph Metadata on pages (summaries)
    - sphinx-togglebutton>=0.3.2
    - sphinx-design>=0.5.0
    - sphinxcontrib-zopeext>=0.4.3  # Documentation of interfaces (mine)
    - sphinxcontrib-bibtex>=2.3.0
    - sphinx-autobuild          # Automatically build and serve (make doc-server)
    - sphinx-comments # Hypothes.is comments and annotations
    - sphinxcontrib-bibtex>=2.4.0
{%-     if cookiecutter.use_latex == "yes" %}
    # https://github.com/executablebooks/sphinx-jupyterbook-latex/issues/97
    - sphinx-jupyterbook-latex
{%-     endif %}    

{%-     if cookiecutter.use_manim == "yes" %}
    - manim>=0.15.0
{%-     endif %}    
{%-   endif %}

{%-   if cookiecutter.use_genbadge == "yes" %}
    - genbadge[all]
{%-   endif %}

{%-   if cookiecutter.use_src == "yes" %}
    # The current project, and dependencies in pyproject.toml.
    - .
    #- -e .                      # If you want an editable install.
{%-   endif %}

{%- else %}

{#-------------------------------------------------------------------#}
{#- Mix of conda (where needed) and pyproject.toml (everything else) #}
{%   if cookiecutter.use_docs == "yes" %}
  # Documentation
  - pandoc
  {%-     if cookiecutter.use_manim == "yes" %}
  - pycairo  # Needed by manim
  - pango    # Needed by manim
  #- manimpango    # Needed by manim
  #- ffmpeg   # Needed by manim
{%-     endif %}    
{%-   endif %}

  - pip  # All pip dependencies should go in pyproject.toml
{%- endif %}
