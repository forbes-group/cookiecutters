"""{{ cookiecutter.project_name }}

{% if cookiecutter.use_course == "yes" %}
This package contains source code for the {{ cookiecutter.term }} {{ cookiecutter.year }} 
offering of {{ cookiecutter.course_name }} at {{ cookiecutter.institution }}.
{% else %}
{{ cookiecutter.one_line_description }}
{% endif %}
"""
