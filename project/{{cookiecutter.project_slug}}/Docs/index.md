<!-- {{ cookiecutter.project_name }}
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->


<!-- Include ../README.md
     If you would like to use the contents of your top-level README.md file here, then
     you can literally include it here with the following:

```{include} ../README.md
``` 

    Note that this may will break `sphinx-autobuild` (`make doc-server`) which will not rebuild
    this index file when ../README.md changes.  See the note at the bottom of the file
    if you want to do this while using sphinx-autobuild.
--> 

# {{ cookiecutter.project_name }}
{% if cookiecutter.use_course == "yes" %}
Welcome to {{ cookiecutter.project_name }}!  This is the main documentation page for the
course.  For more class information, please see the {ref}`sec:sylabus`.

This website, hosted on [Read The Docs]({{ cookiecutter.rtd_url }}), will be used to
collect and display additional information about the course, including:
* {ref}`sec:sylabus`
* {ref}`sec:assignments`
* {ref}`sec:readings`

and various class notes.  These should also be available through the navigation menu
(which might hidden if your display is not sufficiently wide).

These documents are built using [JupyterBook]() (see {ref}`sec:demonstration`) and
include all of the source code needed to generate the figure, plots etc.  For example,
to see how a figure was made, look in the preceding code cell.
{%- if cookiecutter.repository_url %}
The complete source code for this documentation is available at
<{{cookiecutter.repository_url}}>.
{%- endif %} 

{% if cookiecutter.nsf_grant -%}
## Funding Statement
{{ cookiecutter.funding_statement }}
{%- endif %} 



Instructors: the information presented here at the start of the course documentation is
contained in the `Docs/index.md` file, which you should edit to provide an overview of
the course.
{% else %}
Welcome to the documentation for {{ cookiecutter.project_name }}!  To see some of the
features, please see {ref}`sec:demonstration`.

The information presented here at the start of the course documentation is contained in
the `Docs/index.md` file, which you should edit to provide an overview.
{% endif %}
One reasonable option might be to replace this by a literal include of the top-level
`README.md` file with the following code:

````markdown
```{include} ../README.md
``` 
````

```{toctree}
---
maxdepth: 2
caption: "Contents:"
titlesonly:
hidden:
---
{%- if cookiecutter.use_course == "yes" %}
Syllabus
Assignments
{% endif %}
References
```

{%- if cookiecutter.use_course == "yes" %}
```{toctree}
---
maxdepth: 2
caption: "Prerequisites:"
titlesonly:
hidden:
glob:
---
Prerequisites/*
```
{% endif %}

```{toctree}
---
maxdepth: 2
caption: "Miscellaneous:"
hidden:
---
Demonstration
CoCalc
{%- if cookiecutter.use_course == "yes" %}
ClassLog
../InstructorNotes
{% endif %}
README.md <../README>
```

<!-- If you opt to literally include files like ../README.md and would like to be able
     to take advantage of `sphinx-autobuild` (`make doc-server`), then you must make
     sure that you pass the name of any of these files to `sphinx-autobuild` in the
     `Makefile` so that those files will be regenerated.  We do this already for
     `index.md` but leave this note in case you want to do this elsewhere.
     
     Alternatively, you can include them separately and view these directly when editing.
     We do not include this extra toc when we build on RTD or on CoCalc.  We do this
     using the `sphinx.ext.ifconfig extension`:
     
     https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

```{eval-rst}
.. ifconfig:: not on_rtd and not on_cocalc

   .. toctree::
      :maxdepth: 0
      :caption: Top-level Files:
      :titlesonly:
      :hidden:

      README.md <../README>
      InstructorNotes.md <../InstructorNotes>
```
-->

[JupyterBook]: <https://jupyterbook.org>
