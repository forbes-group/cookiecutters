SHELL = /bin/bash

PYTHON_VER ?= 3.9
# https://github.com/pdm-project/pdm/issues/1756#issuecomment-1457979504
PRE ?= VIRTUAL_ENV= CONDA_PREFIX= PDM_VENV_IN_PROJECT=1 PDM_USE_VENV=1 PDM_VENV_BACKEND=virtualenv PDM_IGNORE_SAVED_PYTHON=1
PDM ?= pdm
RUN ?= $(PDM) run

# ------- Top-level targets  -------
# Default prints a help message
help:
	@make usage

usage:
	@echo "$$HELP_MESSAGE"

.PHONY: help usage init sync clean realclean test

# Running make <name>.html will use fswatch to look for modifications in the corresponding
# <name>.md file and will use pandoc to generate the html file, then calling $(OPEN_BROWSER)
# to show this. We then run fswatch to look over this looking for changes.  Once the user
# exits with a keyboard interrupt, the intermediate html file is removed.  We use Safari
# as the default browser on Mac OS X because Chrome-based browsers steal focus.
BROWSER ?= "Brave Browser"
BROWSER ?= "Safari"
OPEN_BROWSER ?= open -g -a $(BROWSER) $@
REFRESH_BROWSER ?= osascript -e 'tell application $(BROWSER) to tell the first tab of its first window to reload'
REFRESH_BROWSER ?= $(OPEN_BROWSER)

PANDOC_FLAGS ?= --preserve-tabs -s --toc
PANDOC ?= pandoc $(PANDOC_FLAGS) --metadata title=\"$*\" $< -o $@
FSWATCH ?= fswatch -e ".*" -i "$<" -o . 
%.html: %.md
	$(PANDOC) && $(OPEN_BROWSER)
	$(FSWATCH) | while read num; do $(PANDOC) && $(REFRESH_BROWSER); done

init: .venv/bin/python$(PYTHON_VER)
	$(PRE) $(PDM) update
	$(PRE) $(PDM) install -G :all

lock:

.venv/bin/python$(PYTHON_VER):
	$(PRE) $(PDM) venv create --force $(PYTHON_VER)
	$(PRE) $(PDM) use -i .venv

clean:
	-find . -name "__pycache__" -exec $(RM) -r {} +
	-find . -name ".ipynb_checkpoints" -exec $(RM) -r {} +
	-$(RM) -r .coverage coverage.xml .pytest_cache build _oven


realclean: clean
	-$(RM) -r __pypackage__ .venv .nox *.egg-info

shell:
	$(PRE) $(PDM) run $(SHELL)

test:
	$(PRE) $(PDM) run pytest $(PYTESTFLAGS) --basetemp="_oven" -nauto -x \
	# tests/test_project.py::test_make_project 

.PHONY: init lock sync clean realclean test

# ----- Usage -----
define HELP_MESSAGE

This Makefile provides several tools to help initialize the project.

Variables:
   ACTIVATE: (= "$(ACTIVATE)")
                     Command to activate a conda environment as `$$(ACTIVATE) <env name>`
                     Defaults to `conda activate`.
   PDM: (= "$(PDM)")
                     Command to run `pdm`.
   PYTHON_VER: (= "$(PYTHON_VER)")
                     Version of python to use in the virtual environment.
   PRE: (= "$(PRE)")
                     Can be used to set environmental variables before commands.
   KERNEL: (= "$(KERNEL)")
                     Name of IPython kernel to install.  Kernel not installed if empty.

Initialization:
   make init         Initialize the environment and kernel.

Testing:
   make test         Runs the general tests.

Maintenance:
   make clean        Call conda clean --all: saves disk space.
   make reallyclean  delete the environments and kernel as well.

endef
export HELP_MESSAGE
