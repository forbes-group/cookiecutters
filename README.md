The Forbes Group Cookiecutter Templates
=======================================
This repository contains [cookiecutter][] templates that we use when starting research
projects and for course documentation.  To simplify maintenance, we generally have
monolithic templates like `project` that include the kitchen sink, but allow you to
disable features through a set of `use_*` yes/no toggles such as:

* `use_docs`: Include a [Jupyter Book][] documentation framework customized for
  deployment on [ReadTheDocs][].
* `use_src`: Include a python package in the `src/{{pkg_name}}` folder that can be installed with
  [pip][].  This will include a `pyproject.toml` file which will initialize and install
  this package.
  * `pkg_name`: The name of the package.  Constructed from the course name or
    `project_slug` if not explicitly defined.
* `use_course`: Include material related to a course, such as a Syllabus, etc.  This
  also changes the default name of the package to something like `phys_555`.  This
  defines the following, which must be specified if `use_course == "no"`:
  * `project_name`: Name of the project.
  * `one_line_description`: Brief description.
  
* `use_cocalc`: Include tools for initializing a project on [CoCalc][].
* `use_gpu`: If `cuda` is installed, then include `cupy` in the environment.
* `use_fftw`: If the `fftw` library is NOT installed, then include `fftw` in the
  environment. *(The better option is to install this for your hardware as the conda
  version is likely not optimized.)*
* `use_manim`: Provide support for [Manim Community][] animations in the documentation.
  Note: only do this if you need Manim support because it can create some tricky
  dependency issues.
* `use_test_assignments`: Setup some tests for automatic grading (experimental.)
  Only works if `use_course` is also `"yes"`.
* `use_genbadge`: If `"yes"`, then install and use [genbadge][] to generate badges for
  your project.  This is needed if you host on [GitLab][] and your project is private.  If
  it is `"no"`, then we assume that the project is public and use [shields.io][].  WIP:
  the badges are not yet fully implemented and tested...
* `use_binder`: If "yes", then enable
  [thebe](https://jupyterbook.org/en/stable/interactive/thebe.html) to make code-cells
  executable on [MyBinder](https://mybinder.org/).
* `use_latex`: If "yes", then include [sphinx-jupyterbook-latex][] and add targets to make
  a [PDF book using LaTeX][].  This requires a working [texlive][] distribution and
  [ImageMagick][] which typically must be installed outside of this project.
* `install_tools`: If "yes", then provide code to install various tools like `black`,
  etc. in a separate environment so that they do not need to be installed in the
  project.


Here are some meta-flags that install certain groups of packages/tools.

* `performance`: If "yes", then install some performance tools like `numba`. `numexpr`,
  etc.  (Currently, this is done in `environment.yaml`.  Some testing is needed for
  similar effects in `pyproject.toml`.
* `notebook`: If "yes", then installs tools like [black][] and
  [jupyter_nbextensions_configurator][] to aid running Jupyter notebooks from the shell.

Some other configurations specify external repository addresses for documentation and
source code:

* `rtd_url`: Location of the documentation on [ReadTheDocs][].

   E.g.: `"https://wsu-phys-581-computation.readthedocs.io/en/latest/"`
   
* `gitlab_url`: [GitLab][] repo.

   E.g.: `"https://gitlab.com/wsu-courses/physics-581-physics-inspired-computation"`
   
* `github_url`: [GitHub][] repo.
  
   E.g.: `"https://github.com/WSU-Physics-Courses/physics-581-physics-inspired-computation"`

Instructions
============

## TL;DR

The basic idea is to install and run `cookiecutter` on the appropriate template:

```bash
pipx install cookiecutter
mkdir my-project; cd my-project
cookiecutter git+https://gitlab.com/forbes-group/cookiecutters.git --directory project
```

However, we do a few things differently than you might be used to, so we recommend the
following strategy:

### Starting From Scratch

1. Our workflow is to use a version control system (VCS) like [Mercurial][] or [Git][] and
   to update the repository when the templates change, so we recommend at least working
   in a clean folder (which will be modified in place).  If you are starting locally, then:

   ```bash
   mkdir my_project
   cd my_project
   hg init
   ```
   
   Alternatively, if you are going to host on [GitLab][] or [GitHub][], first create
   that project, then clone it here (important if you use [Hg-Git][]).  E.g.:
   
   ```bash
   hg clone git@gitlab.com:wsu-courses/iscimath-583-learning-from-images-and-signals.git my_project
   cd my_project
   ```

2. Our templates are monolithic, so you probably don't want to answer all of the
   questions.  Instead, download the template `*.yaml` file that most closely matches
   your needs and use it as an input file.  If using VCS, add this to your repository as
   the `default` or `main` branch.

   ```bash
   wget -O .cookiecutter.yaml https://gitlab.com/forbes-group/cookiecutters/-/raw/main/project/code-repo.yaml?inline=false
   vi .cookiecutter.yaml   # Edit as needed.
   ```

   Here you need to edit the `.cookiecutter.yaml`.  Most of the entries should be
   self-explanatory, but a few require additional work:
   * `gitlab_url`: You may want to create a [GitLab][] repo for the project.
     Keep it empty (i.e. don't choose a license yet or add a README file.)  For courses
     use a course project like [WSU Courses](https://gitlab.com/wsu-courses/) which
     benefits from the [GitLab for
     Education](https://about.gitlab.com/solutions/education/) program.
   * `github_url`: I usually create a [GitHub][] mirror using a [personal access
     token][GitLab mirroring tokens] **Settings->Developer settings->Personal access
     tokens->Fine-grained tokens**.  Enter the URL like this:
     `https://<your_access_token>@github.com/WSU-Physics-Courses/iscimath-583-...git`
     Be sure to disable issues, wikis, etc. here under **Settings->Features** on
     [GitHub][] so that all request go to the primary repo at [GitLab][].
   * `resources_url`: I usually create another private [GitLab][] repo for course
     materials that should only be distributed to the class.  Usually not needed for
     code projects.
   * `rtd`: Slug from [ReadTheDocs][]: import this from your [GitLab][] repo.  If you
     do this a lot, connect your accounts under [Settings->Connected
     Services](https://readthedocs.org/accounts/social/connections/).  On import, make the
     **Name:** somewhat short to get a small slug (long slugs can cause problems).  You
     can restore the name later.
   * `cocalc_project` and `cocalc_course_project`: if you manage a [CoCalc][] course,
     then create it in the  `cocalc_course_project`, and give the Shared Project slug as
     the `cocalc_project`.
3. Added the project details to your version control under the `default` (Mercurial) or
   `main` (Git) branch:
   
   ```bash
   hg add .cookiecutter.yaml
   hg com -m "Initial commit of .cookiecutter.yaml config file."
   ```
4. Run `cookiecutter` to generate the project.
   
   **Important:** make sure there is nothing in the folder except for
   `.cookiecutter.yaml` and other dot files/folders (`.hg`, `.git` etc.), otherwise the
   template will be installed in a sub-folder.
   
   ```bash
   cookiecutter git+https://gitlab.com/forbes-group/cookiecutters.git \
                --directory project                                   \
                --config-file .cookiecutter.yaml                      \
                --overwrite-if-exists                                 \
                --no-input
   ```
5. If using VCS, commit the results to a special `cookiecutter-base` branch and then
   merge these changes back into your main branch:

   ```bash
   hg branch cookiecutter-base
   hg add
   hg com -m "BASE: Initial cookiecutter template"
   hg up default
   hg merge cookiecutter-base
   hg com -m "Merge in cookiecutter updates"
   hg bookmark main # If using hg-git
   ```
6. If, in the future, you would like to take advantage of updates to the cookiecutter
   template, you can do so by updating to the `cookiecutter-base` branch, re-generating
   the project, committing, and then merging these.  Hopefully the VCS will be able to
   track your changes and you will have minimal conflicts.

   ```bash
   hg com   # Make sure you save your work!
   hg up cookiecutter-base
   rm -rf Docs/_*    # See note about issue 1176
   cookiecutter git+https://gitlab.com/forbes-group/cookiecutters.git \
                --directory project                                   \
                --config-file .cookiecutter.yaml                      \
                --overwrite-if-exists                                 \
                --no-input
   hg commit --addremove -m "BASE: Updated cookiecutter skeleton"
   hg up default
   hg merge cookiecutter-base
   hg com -m "Merge in cookiecutter updates"
   ```
   
   Note: Due to [issue 1176][], if certain nested folders exist, `--overwrite-if-exists`
   may fail.  The solution is simply to remove these folders before updating.  Since
   `cookiecutter-base` is a clean branch, this is fine.  (Note: we can't remove
   everything since we need `.cookiecutter.yaml`)

### Existing Projects

If you are starting from an exiting repository, you will need to create the
`cookiecuter-base` branch.  With mercurial you can do this as follows.  (Note: here we
include the `.cookiecutter.yaml` file in the first commit of this branch.)

For reasons discussed below, you should do this in a fresh clone, otherwise you may run
into problem when you run `cookiecutter`.

```bash
hg clone <your repo> <tmp>; cd <tmp>
hg up null
hg branch cookiecutter-base
hg add .cookiecutter.yaml
hg com -m "BASE: Initial commit of .cookiecutter.yaml"
cookiecutter git+https://gitlab.com/forbes-group/cookiecutters.git \
             --directory project                                   \
             --config-file .cookiecutter.yaml                      \
             --overwrite-if-exists                                 \
             --no-input
hg add
hg com -m "BASE: Initial cookiecutter template"
hg up default
hg merge cookiecutter-base   # This will require quite a bit of work...
hg com -m "Merge in cookiecutter updates"
hg bookmark main # If using hg-git
```

[GitLab mirroring tokens]: https://docs.gitlab.com/ee/user/project/repository/mirror/push.html#set-up-a-push-mirror-from-gitlab-to-github



Details
=======

To properly answer several questions, however, first requires some work.  In particular,
we assume that you will host/mirror your project on various servers, including [GitHub][],
[GitLab][], and [ReadTheDocs][].  For these aspects of the templates to work properly, you
must give appropriate values, and the exact URL for some of these requires actually
generating the projects.  Thus, we recommend you proceed as follows:

1. Create an empty repository for your project wherever you would like to host it:
   [GitHub][], [GitLab][], [Heptapod][], or a custom site like https://hg.iscimath.org.  A
   couple of things to consider:
   
   * [Heptapod][] is the only viable option I know of if you want to use Mercurial.  You
     can host free and open-source software (FOSS) here, but no private projects.
     Currently, you must [manually request](https://foss.heptapod.net/projects/new) new
     project to ensure they meet the FOSS criterion.  As this is based on [GitLab][],
     there is a [Heptapod Community Edition][] you can run yourself.  We run a private
     instance of this at https://hg.iscimath.org where we host private repositories.
     
     [Heptapod][] did the FOSS community a great service by providing a [Bitbucket import][]
     when Bitbucket [abandoned the mercurial community][] on short notice without
     providing any migration options, and breaking thousands of links to information
     about FOSS in issues, contrary to their claims that ["we're very commited to
     continuing support for mercurial"][] etc., *(For this reason, I **strongly**
     discourage anyone from working with Bitbucket or Atlassian if there is any
     reasonable alternative.)*
   * [GitLab][] provides an open-source [GitLab Community Edition][] (CE) that you can host
     on your own hardware or [AWS][].  Thus, you have complete control of your data, and
     can host it in the future.
   * [GitHub][] is a bit more popular, so if you want to attract collaborators, this may
     be the better choice.  For me, however, the lack of a community edition is a
     no-go.  I feel I can get all the advantages of [GitHub][] by mirroring here.  Thus, I
     recommend using either [Heptapod][] or [GitLab][] -- both of which can mirror to
     [GitHub][].
   
   Checklist:
   
   * [ ] Make sure the project is public (so we can import it to [Read the Docs][]).
   * [ ] I recommend using one repository per course.  I add a bookmark/branch to
         indicate the last revision relevant for a given year for historical purposes,
         but keep updating the projects assuming that they get better and better.  (That
         is the plan anyway!)

2. Mirror the project on [GitLab][] and/or [GitHub][].  This is not strictly needed unless
   you host privately (in which case, you will not be able to automatically sync with
   [Read the Docs][]), but will allow you to use the CI tools on these sites, and increase
   visibility.
   
3. Import your project to [Read the Docs][].

   **Important:** Just after import, change the **Project Name** to a short name like
   `wsu-phys-581-computation` so that [Read the Docs][] will generate a slug based on your
   repositories name, and if this gets too long, you can [run into
   issues](https://github.com/readthedocs/readthedocs.org/issues/8565).  You can change
   this to something more descriptive once the slug is generated.
   
   Use the resulting URL for the `rtd` entry.
   
4. Run [`cookiecutter`][] to generate your project.
5. Initialize your repository.
6. Add all the files and commit them to a "cookiecutter" topic or branch.  Use this
   branch to update when you re-run [cookiecutter][], then merge. 
7. Continue working in your `main` branch.

## Configuration Options

Here is some information about parameters in `cookiecutter.json`.  We try to provide
reasonable defaults, but several may need customization.

* `project_slug`: This will be used for the directory name.
* `pkg_name`: Python-importable name for your package.  This will be a directory in the
  `src/` folder at the top level of the project, and installed in the active
  environment.
* `env_name`: Name of the [Conda][] environment and [IPython kernel][] that will be
  generated.  By default, the same as `pkg_name` but with hyphens.
* `gitlab`, `gitlab_git`: [GitLab][] project URL and corresponding URI for cloning the repo.
* `github`, `github_git`: [GitHub][] project URL and corresponding URI for cloning the repo.
* `rtd`: [Read the Docs][] documentation URL.
* `get_resources`: If you have a repository with resources like papers, textbooks,
  etc. then include the command to get it and put it in `_ext/Resources`.  If not, leave
  this empty.
* `cocalc_project`: The id for the class shared project on [CoCalc][].  (This is the
  string of numbers and letters like `"74852aba-2484-4210-9cf0-e7902e5838f4"` that
  appears in the project URL.)  If this is defined, then we will include the tools for
  initializing [CoCalc][] in the `Makefile`.
* `cocalc_course_project`: The id for the [CoCalc][] project containing the `.course`
  files.  The current template assume this to have course files in the format:
  
  * `Archive/{{deptartment}}{{course_number}}/{{year}}-{{dept}}-{{course_number}}-{{term}}.course`
  
  For example:
  
  ```bash
  README.md
  Archive/
  |-- Mathematics589
  |   |-- 2022-Math-589-Spring.course
  |   ...
  |-- Physics450
  |   |-- 2020-Phys-450-Fall.course
  |   ...
  |-- Physics521
  |   |-- 2020-Phys-521-Fall.course
  |   |-- 2021-Phys-521-Fall.course
  |   ...
  |-- Physics581
  |   |-- 2021-Phys-581-Fall.course
  |   ...
  ...
  ```
  
  Use symlinks to the top level for more informative interface, and document these in
  the `README.md` file.
  
Some of the options are computed from these.  One can override these, but they often
have special meaning.

* `dept`: Shortened form of `department` for use in environment and package names etc.  For example:
  `Phys`, `Math`, etc.
  
**WSU Course Specific:**

* `institution`, `department`, `campus`, `course_number`, `course_section`: These are
  used in various places to construct values.  These have been designed for the
  `"Physics"` department in `"Pullman"` and may need customization for other locations.
  Please file [an issue][] with details if the heuristics do not work for you.
* `personnel`: Dictionary containing `instructors` and `assistants` lists.  These are
  typeset as is, so should include email addresses etc.
* `office`:
* `term`: Assumed to be one of `"Spring"`, `"Summer"`, or `"Fall"`.
* `zoom_*`: We generate Zoom links without the password for security.  Edit these in the
  documentation if you want to provide a direct link (but keep in mind that this will be
  available for public consumption if you publish your course!)
  
* `catalogue_entry`, `course_page`: We try to generate these values linking to
  the course catalog etc., but the rules are not well-defined. Please file [an issue][]
  with details if the heuristics do not work for you.
* `class_number`: you need to look this up on the `course_page`.

The variables and several others will be pulled into a single dictionary `syllabus` in
the final `conf.py` file to make things cleaner for the user and to not over-pollute the
`myst_substitutions` namespace.

# Dependency Management

Our strategy is to use ([micromamba][]/[conda][]/[anaconda-project][]/nothing) to
install an appropriate version of Python and any binary dependencies, then to use
([PDM][]/[Poetry][]/[Pip][]/nothing) to install the remaining python dependencies.
Note: we currently focus on the first set of options - others may not be as well tested
(or may fail):
* `use_conda: "micromamba"`
* `use_pyproject: "pdm"`

To Do: Look at https://scientific-python.org/ and see what they do.

This approach has the following advantages:

* We can use [micromamba][] to install difficult packages like [pyFFTW][] that depend on
  binaries.
* As opposed to just using [micromamba][], our approach allows one to take advantage the
  more sophisticated (sometimes rigid) dependency resolution algorithms in [PDM][] or
  [poetry][], which can improve reliability.
* We can add custom commands as targets in `Makefile` (see below).

Dependencies should probably be managed in the package itself by running i.e. `pdm
update`, but some packages can be specified in the `.cookiecutter.yaml` file:

* `conda_dependencies: [["pandoc"]]`: These must be installed with `micromamba`.
* `dependencies: [["scipy", "matplotlib", "sympy"]]`: These could be installed with
 either [micromamba][] or [Pip][].
* `pip_dependencies: [["mmf_setup"]]`: These must be installed with [Pip][].
* `performance: "yes"`: Installs `numba`, `pyfftw`, `numexpr` etc. to provide
  high-performance computing options.
* `notebook: "yes"`: Installs [Jupyter][] notebook and various configuration tools for
  use in the shell.
* `use_manim: "yes"`: Includes additional tools to support [Manim Community][].
* `use_docs: "yes"`: Includes many tools like [Sphinx][] for building documentation.

Most of these are organized in `pyproject.toml` (if using) under the following groups:

Our current strategy is that `environment.yaml` does not install any `pip:`
dependencies.  This should be done directly with `pip` or `pdm` as needed.
This is done in the `Makefile` where one can also customize extras like `[docs]` and
`[tests]` with `EXTRAS` and make the install `EDITABLE`.

**Rational:**

1. There is no way to customize extras in `environment.yaml`, so we would be forced to
   pick one, then use `pip` etc. later to change these.  We could just use the bare
   package `.` but this seems cleaner.
2. Users can eschew the use of `conda` or `micromamba` if they want to use some other
   way to get those binary dependencies.
3. We found a simple way to support this with RTD. (See `.readthedocs.yaml`)

> Note: We previously used `anaconda-project.yaml` for this, but no longer support this
> fully.  [Anaconda-project][] has the potential to replace the `Makefile` and to provide
> some nice features for managing dependencies on different platforms, but the added
> complication has become a maintenance burden.  With this approach, and the following
> aliases that are now provided as targets in `Makefile`:
> 
> ```bash
> alias ap = "anaconda-project"
> alias apr = "anaconda-project run"
> ```
> 
> * `apr shell`: Starts a new shell running in our environment, like [`poetry
>   shell`](https://python-poetry.org/docs/cli/#shell).  See also [this
>   discussion][pdm#1758] where I ask if [PDM][] provides similar functionality.
> * `apr init`: Anaconda-project cannot currently [install .][ap#332], so to take
>   advantage of [poetry][] or [PDM][], we run these commands here.  We also use this to
>   install a Jupyter kernel for notebooks, and any other customization needed.
> * The [anaconda-project][] configuration file `anaconda-project.yaml` can be used as an
>   `environment.yaml` file for [Conda][] with one small change: replacing the `packages:`
>   section with `depenencies:` (see anaconda-project [#194][ap#194] and [#337][ap#337].

## [PDM][]

[PDM][] is a standards-compliant dependency management tool supported by the [Python
pakaging authority][PyPA].  You can use it to manage the dependencies your
`pyproject.toml` and to generate lock files with precise versions.  However, there are
some very annoying issues and gotchas.

* It can be quite difficult to be sure that `pdm` is using the correct environment (see
  e.g. [pdm #1763][] and [pdm #1758][]). One safe way is to make sure you run `pdm` from
  within the shell:
  
  ```bash
  make shell
  pdm update
  ...
  ```

The general strategy is to generate a lock file with:

```bash
pdm update
```

This will try to find a specific set of packages consistent with all requirements
specified in `pyproject.toml` as well as their sub-dependencies.  In practice, this can
get stuck, especially if you have complex

### Multiple Versions of Python

If you are developing a library that is intended to support multiple versions of python,
then good luck!  There is limited support for doing this, but the general idea is to
try, fail, and then add markers (see also [pdm #46][]).  Here is how I proceeded to find
the chain of versions required for `ipykernel`:

1. I created an new project with `pdm` and a `pyproject.toml` file like this:

   ```toml
   [project]
   name = "trial"
   version = "0.1.0"
   description = "Default template for PDM package"
   authors = [
       {name = "Michael McNeil Forbes",email = "michael.forbes+numpy@gmail.com"},
   ]
   dependencies = [
       #'ipykernel >= 6.16.2; python_version < "3.8"',
       #'ipykernel >= 6.29.0; python_version >= "3.8"',
       #'ipython >= 7.16.3; python_version < "3.7"',
       #'ipython >= 7.34.0; python_version < "3.8"',
       #'ipython >= 8.12.3; python_version < "3.9"',
       #'ipython >= 8.18.1; python_version < "3.10"',
       #'ipython >= 8.20.0; python_version >= "3.10"',
       #'setuptools >= 68.0.0; python_version < "3.8"',
       #'setuptools >= 69.0.3; python_version >= "3.8"',
       #'setuptools-scm >= 7.1.0; python_version < "3.8"',
       #'setuptools-scm >= 8.0.4; python_version >= "3.8"',
   ]
   requires-python = ">=3.7,<3.12"
   readme = "README.md"
   license = {text = "MIT"}

   [build-system]
   requires = ["pdm-backend"]
   build-backend = "pdm.backend"

   [tool.pdm]
   package-type = "library"
   ```
2. I run `pdm add ipykernel`.  This gives a bunch of errors like:

   ```bash
   ... PackageWarning: Skipping ipykernel@6.29.0 because it requires Python>=3.8 but the
       project claims to work with Python>=3.7,<3.12...
   ```
   
   This inserts `"ipykernel>=6.16.2",` into the `dependencies` section, and I use this
   with a marker (see above) and then repeat with `requires-python = ">=3.8,<3.12"`,
   rerunning `pdm add ipykernel`.  (Sometimes you might need something more restrictive
   like `requires-python = ">=3.8,<3.9"`.)
3. Once I get a complete set of markers for `ipykernel` I try uncomment them and run
   `pdm lock`.  This will either succeed (yay!) or fail with another `PackageWarning`:
   
   ```bash
   ... PackageWarning: Skipping ipython@8.20.0 because it requires Python>=3.10...
   ```
   
   I then repeat the process, restricting versions and running `pdm add ipython` to get
   the `ipython` chain.
   
It seems this could be fairly easily automated... not sure why it is not done.  Maybe I
need to write an extension.

# Make and Makefiles

To help users get started, we also provide a `Makefile` which will do everything needed
by running

```bash
make init
```

The `Makefile` provides us with a place to collect wisdom about platform-specific
difficulties etc.  We also use to provide a rapid start on [CoCalc][] (see below).
Since we rely heavily on this `Makefile`, here are some tips for debugging.

* Run `make` without arguments for help:

  ```bash
  $ make

  This Makefile provides several tools to help initialize the project.  It is primarly designed
  to help get a CoCalc project up an runnning, but should work on other platforms.
  
  Variables:
  ...
  ```

* Run `make -n` to see what would happen (without doing it) or `make -nd` to see
  detailed information about why certain targets are being rebuilt.

  ```bash
  $ make -n pipx
  command -v pipx || \
    echo "I do not know how to make \"pipx\". If it is a command, please install (e.g. with apt-get)."
  ```
  *(This is a check to see if the command `pipx` exists.  It is used as a target to
  throw an error if you don't have `pipx` installed.  If you do, then nothing will happen.)*


### Questions:

* [Micromamba][] or [Miniconda][]?  [Micromamba][] is very fast and has been working
  well - it is our current default.

[Mamba]: <https://mamba.readthedocs.io/en/latest>
[micromamba]: <https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html>
[conda-forge]: <https://conda-forge.org/>
[pyFFTW]: <https://github.com/pyFFTW/pyFFTW>
[ap#194]: <https://github.com/Anaconda-Platform/anaconda-project/issues/194>
[ap#332]: <https://github.com/Anaconda-Platform/anaconda-project/issues/332>
[ap#337]: <https://github.com/Anaconda-Platform/anaconda-project/issues/337>
[conda#11895]: <https://github.com/conda/conda/issues/11895>
[pdm#1758]: <https://github.com/pdm-project/pdm/discussions/1758>

### Python Version

Which version of python should you support? Python 3.11 works well and has good
performance.  At the time of writing, many packages do not properly support 3.12: e.g. [Numba does
not work with 3.12][].

[Numba does not work with 3.12]: <https://github.com/numba/numba/issues/9197>

# Docs

The project initializes a [Jupyter Book][] based set of documentation in the `Docs/`
folder, but uses the [Jupyter Book with Sphinx][] option that enables all features
directly with [Sphinx][] rather than relying on the `jupyter-book` command.  Sample
documentation is provided written in [MyST][] -- a version of [Markdown][] (strictly a
superset of [CommonMark][]) with extensions that support the added features of
[reStructuredText][] needed by [Sphinx][] for a bunch of features like cross-references,
admonitions, sidebars, etc.  For details, please see:

* [The Executable Books Project][]

Here are some notes:

## [Jupytext][]

We use [Jupytext][] to synchronize the markdown files with Jupyter Notebooks.  When
developing code examples, running as a Jupyter notebook can be very convenient.  When
you are done, run `make sync` if needed (i.e. on [CoCalc][]).  I usually start new
documentation by copying a previous markdown file: if you start from a Jupyter notebook,
pair with `myst`, and look at the metadata carefully.  (For example, 

https://jupytext.readthedocs.io/en/latest/formats.html?highlight=pygments_lexer#myst-markdown

## Admonitions, Margin Notes, Sidebars, Etc.

I like including extra material in margin notes, or quasi-hidden **Do It!** examples as
dropdown admonitions so that relevant information is at the right place, but does not
distract from the main flow of reading.  This generally works well with code like:

````markdown
```{margin}
This is a margin note
```
The note above will appear to the right of this text (in the
actual docs, not in this README file).

```{admonition} Do It! What does a circle look like in the $L_1$ norm?
:class: dropdown

It looks like a diamond.  Note: this will be hidden, giving students a
chance to try before looking for the answer.
```
````

One limitation is that [code cells cannot be simply
included](https://github.com/executablebooks/meta/discussions/159).  The [sphinx-example][]

[sphinx-example]: <>




## Substitutions

We insert all of the above variables into the `myst_substitution` dictionary in
`Docs/conf.py` so that these can be used directly with the [MyST substitutions][]
extension.  This is a bit of a problem for directly editing and rendering the markdown
files, but allows one to change these properties globally going forward (i.e. class
times, instructors etc.)  We also include all of the variables 

Note to template developers: In order to prevent [Jinja][] from processing these
substitutions, we must [escape][] the substitutions appropriately with code like one of
the following:

    ```markdown
    {{ '{{ instructor }}' }}
    {% raw %}{{ instructor }}{% endraw %}
    ```

[possible `subs` filter]: <https://github.com/executablebooks/MyST-Parser/issues/279#issuecomment-1019948255>

# Key Reference

Here is a complete list of the keys in `cookiecutter.json`.  If only a limited number of
options are allowed, these are specified, otherwise a typical value is shown.  Some
features are experimental and/or untested (WIP).

## Toggles

* `use_docs: ["yes", "no"]`:
  Include the [Jupyter Book][] documentation framework.
* `use_cocalc ["yes", "no"]`: 
  Include tools for initializing a project on [CoCalc][].
* `use_course: ["yes", "no"]`: 
  Include material related to a course, such as a Syllabus, etc.  This also changes the
  default name of the package to something like `phys_555_2022`.
* `use_genbadge: ["yes", "no"]` (WIP):
  Install and use [genbadge][] to generate badges for your project.  This is needed if
  you host on [GitLab][] and your project is private.  If it is `"no"`, then we assume
  that the project is public and use [shields.io][].
* `use_latex: ["yes", "no"]` (WIP):
  If "yes", then include [sphinx-jupyterbook-latex][] and add targets to make a [PDF
  book using LaTeX][].  This requires a working [texlive][] distribution and
  [ImageMagick][] which typically must be installed outside of this project.
* `use_myst_substitutions: ["yes", "no"]` (WIP): Enable MyST substitutions.  Not
  recommended due to issues.

## Experimental Features

The following features are incomplete or known to cause problems.

* `use_experimental: ["yes", "no"]` (WIP):
  Used as a catchall to remove all experimental code if `"no"`.
* `use_binder: ["yes", "no"]` (WIP):
  Enable [thebe](https://jupyterbook.org/en/stable/interactive/thebe.html) to make
  code-cells executable on [MyBinder](https://mybinder.org/).

## URLs and Links

* `rtd_url`: Location of the documentation on [ReadTheDocs][].
   E.g.: `"https://wsu-phys-581-computation.readthedocs.io/en/latest/"`
* `gitlab_url`: [GitLab][] repo.
   E.g.: `"https://gitlab.com/wsu-courses/physics-581-physics-inspired-computation"`
* `github_url`: [GitHub][] repo.
   E.g.: `"https://github.com/WSU-Physics-Courses/physics-581-physics-inspired-computation"`
* `perusall_url`: [Perusall][] site for readings.
   E.g. `"https://app.perusall.com/courses/2021-fall-physics-581-pullm-1-02-01665-adv-topics-in-physics/"`

## Project

* `use_anaconda_project: ["yes", "no"]`: 
  Use [anaconda-project][] to control the base build.  Currently this is needed but may 
  be removed in the future to support pure python projects. 
* `use_poetry: ["yes", "no"]`: 
  Use [poetry][] to manage pure python dependencies.
* `use_pdm: ["yes", "no"]`: 
  Use [PDM][] to manage pure python dependencies.
* `python_minver: "3.9"`: 
  Minimum python version allowed.
* `python_maxver: "3.12"`: 
  Maximum python version not allowed.  (I.e. `python_minver <= ver < python_maxver`.)
* `make_tools: ["yes", "no"]`: 
  Include `make tools` target that installs tools into a special environment
  `envs/tools` or with `pipx`.  Requirements for these tools go in
  `.tools/requirements.<tool>.txt` or `.tools/environment.tools.yaml` files.
* `dependencies: [["sympy", "scipy", "matplotlib"]]`: 
  List of dependencies to install.  If using [anaconda-project][], then these will be
  installed with [conda][] and should have the appropriate channel included if needed,
  i.e. `"conda-forge::jupyter_console"`.
* `pip_dependencies: [["mmf_setup"]]`:
  List of dependencies that should be installed with [pip][], [poetry][], or [pdm][].

## Docs

These keys only have meaning if `use_Docs == "yes"`.

* `use_manim: ["yes", "no"]`: 
  Provide support for [Manim Community][] animations in the documentation. Note: only do
  this if you need Manim support because it can create some tricky dependency issues.


## Course

These keys only have meaning if `use_course == "yes"`.

* `use_test_assignments`: Setup some tests for automatic grading (experimental.)
* `institution`: Institution course is taught at.
* `department`: `["Physics", "Mathematics"]`
* `campus`: `"Pullman"`
* `campus_state`: `"WA"`
* `dept": "{{ cookiecutter.department|truncate(4,True,'',0) }}",
* `course_number": "581",
* `course_section": "02",
* `course_level": "{% if cookiecutter.course_number.startswith('5') %}graduate{% else %}undergraduate{% endif %}",
* `year": "2022",
* `term": ["Spring", "Summer", "Fall"],
* `credits": "3",
* `prerequisites": "Linear Algebra",
* `grading_statement": "Grade based on assignments and project presentation.",
* `class_time": "MWF, 12:10pm - 1pm",
* `class_room": "Spark 223",


## Misc
* `zoom_number": "957 9571 0263",
* `zoom_url": "https://wsu.zoom.us/j/{{ cookiecutter.zoom_number|replace(' ', '') }}",
* `canvas_url": "", 
* `catalogue_entry": "https://catalog.wsu.edu/{{ cookiecutter.campus }}/Courses/ByList/{{ {'Physics': 'PHYSICS', 'Mathematics': 'MATH'}[cookiecutter.department] }}/{{ cookiecutter.course_number }}",
* `wsu_class_number": "01665",
* `author": "Michael McNeil Forbes",
* `author_email": "m.forbes+{{ cookiecutter.course_number }}@wsu.edu",

* `office": "947F Webster",
* `phone": "(509) 335-6125",
* `office_hours": "MWF, 1pm - 2pm, Spark 223 (after class) or by appointment",
* `title": "Physics Inspired Computational Techniques",
* `short_title": "{{ cookiecutter.title }}",
* `course_name": "{{ cookiecutter.dept }} {{ cookiecutter.course_number }}: {{ cookiecutter.title }}",
* `project_name": "{{ cookiecutter.dept }} {{ cookiecutter.course_number }} - {{ cookiecutter.short_title }}",
* `project_name_year": "{{ cookiecutter.project_name }} ({{ cookiecutter.term }} {{ cookiecutter.year }})",
* `project_slug": "{{ cookiecutter.project_name|lower|replace(' - ', '-')|replace(' ', '-') }}",
* `pkg_name": "{% if cookiecutter.use_course == 'yes' %}{{ cookiecutter.dept|lower }}_{{ cookiecutter.course_number }}{% else %}{{ cookiecutter.project_slug|lower|replace('-', '_') }}{% endif %}",
* `env_name": "{{ cookiecutter.pkg_name|replace('_', '-') }}",
* `version": "0.1",
* `one_line_description": "WSU Course {{ cookiecutter.department }} {{ cookiecutter.course_number }}: {{ cookiecutter.title }} taught {{ cookiecutter.term }} {{ cookiecutter.year }}",

* `cocalc_project": "74852aba-2484-4210-9cf0-e7902e5838f4",
* `cocalc_id": "{{cookiecutter.cocalc_project|replace('-', '')}}",
* `cocalc_course_project": "c31d20a3-b0af-4bf7-a951-aa93a64395f6",
* `cocalc_url": "https://cocalc.com/projects/{{ cookiecutter.cocalc_project }}/files/",
* `cocalc_course_url": "https://cocalc.com/projects/{{ cookiecutter.cocalc_course_project }}/files/Archive/{{ cookiecutter.department }}{{ cookiecutter.course_number }}/{{ cookiecutter.year }}-{{ cookiecutter.dept }}-{{ cookiecutter.course_number }}-{{ cookiecutter.term }}.course",
* `gitlab_url": "https://gitlab.com/wsu-courses/physics-581-physics-inspired-computation",
* `gitlab_git": "{{ cookiecutter.gitlab_url }}.git",
* `github_url": "https://github.com/WSU-Physics-Courses/physics-581-physics-inspired-computation",
* `github_git": "{{ cookiecutter.github_url }}.git",
* `use_genbadge": ["yes", "no"],
* `repository_url": "{{ cookiecutter.gitlab_url }}",
* `issues_url": "{{ cookiecutter.repository_url }}/issues",
* `resources_url": "https://gitlab.com/wsu-courses/physics-581-physics-inspired-computation_resources",
* `resources_url": "",
* `get_resources": "{% if cookiecutter.resources_url %}git clone {{ cookiecutter.resources_url | replace('https://gitlab.com/', 'git@gitlab.com:') }}.git _ext/Resources{% endif %}",
* `rtd": "",
* `rtd_url": "{% if cookiecutter.rtd %}https://{{cookiecutter.rtd}}.readthedocs.io/en/latest{% endif %}",
* `rtd_badge": "{% if cookiecutter.rtd %}https://readthedocs.org/projects/{{cookiecutter.rtd}}/badge/?version=latest{% endif %}",
* `nsf_grant": "",
* `funding_statement": "{% if cookiecutter.nsf_grant %}<a href=\"https://www.nsf.gov\"><img width=\"10%\"\nsrc=\"https://www.nsf.gov/images/logos/NSF_4-Color_bitmap_Logo.png\" />\n</a>\n<br>\n\nSome of the material presented here is based upon work supported by the National Science\nFoundation under [Grant Number {{cookiecutter.nsf_grant}}](https://www.nsf.gov/awardsearch/showAward?AWD_ID={{cookiecutter.nsf_grant}}). Any opinions, findings, and conclusions or\nrecommendations expressed in this material are those of the author(s) and do not\nnecessarily reflect the views of the National Science Foundation.\n{% endif %}",
 
* `course_page": "https://schedules.wsu.edu/List/{{ cookiecutter.campus }}/{{ cookiecutter.year }}{{ {'Spring': '1', 'Summer': '2', 'Fall': '3'}[cookiecutter.term] }}/{{ {'Physics': 'Phys', 'Mathematics': 'Math'}[cookiecutter.department] }}/{{ cookiecutter.course_number }}/{{ cookiecutter.course_section }}",
* `personnel": {
  "instructors": ["{{ cookiecutter.author }} [`{{ cookiecutter.author_email }}`](mailto:{{ cookiecutter.author_email }})"],
  "assistants": []
 },
* `syllabus": {
  "instructors": "{{ cookiecutter.personnel.instructors }}",
  "assistants": "{{ cookiecutter.personnel.assistants }}",
  "institution": "{{ cookiecutter.institution }}",
  "campus": "{{ cookiecutter.campus }}",
  "campus_state": "{{ cookiecutter.campus_state }}",
  "dept": "{{ cookiecutter.dept }}",
  "course_number": "{{ cookiecutter.course_number }}",
  "course_section": "{{ cookiecutter.course_section }}",
  "year": "{{ cookiecutter.year }}",
  "term": "{{ cookiecutter.term }}",
  "class_time": "{{ cookiecutter.class_time }}",
  "class_room": "{{ cookiecutter.class_room }}",
  "perusall_url": "{{ cookiecutter.perusall_url }}",
  "zoom_number": "{{ cookiecutter.zoom_number }}",
  "zoom_url": "{{ cookiecutter.zoom_url }}",
  "canvas_url": "{{ cookiecutter.canvas_url }}",
  "catalogue_entry": "{{ cookiecutter.catalogue_entry }}",
  "course_page": "{{ cookiecutter.course_page }}",
  "wsu_class_number": "{{ cookiecutter.wsu_class_number }}",
  "office": "{{ cookiecutter.office }}",
  "office_hours": "{{ cookiecutter.office_hours }}",
  "phone": "{{ cookiecutter.phone }}",
  "department": "{{ cookiecutter.department }}",
  "course_name": "{{ cookiecutter.course_name }}",
  "cocalc_project": "{{ cookiecutter.cocalc_project }}",
  "cocalc_course_project": "{{ cookiecutter.cocalc_course_project }}",
  "cocalc_url": "{{ cookiecutter.cocalc_url }}",
  "cocalc_course_url": "{{ cookiecutter.cocalc_course_url }}",
  "credits": "{{ cookiecutter.credits }}",
  "prerequisites": "{{ cookiecutter.prerequisites }}",
  "grading_statement": "{{ cookiecutter.grading_statement }}"
 },
* `_symlinks": {
  "Docs/InstructorNotes.md": "../InstructorNotes.md",
  "Docs/README.md": "../README.md"},
* `_copy_without_render": [
  "Docs/*.bib",
  "Docs/_*",
  "Docs/Notes/*"
 ]
}

# Development

Here are some notes for developers updating and testing these templates.  For more
background about the philosophy and ideas behind our choices, see [mmfutils/Notes.md][].


We are using
[pytest-cookies][] for testing.


```bash
```

<!-- global markdown links -->
<!-- The intention is to replace these links programmatically with a hook -->
[mmfutils/Notes.md]: <https://github.com/forbes-group/mmfutils/blob/branch/default/Notes.md>
[anaconda-project]: <https://anaconda-project.readthedocs.io/en/latest/>
[pytest-cookies]: <https://github.com/hackebrot/pytest-cookies>
[Conda]: <https://docs.conda.io/en/latest/>
[PDM]: <https://pdm.fming.dev/latest/>
[PyPA]: <https://www.pypa.io/en/latest/>
[Poetry]: <https://python-poetry.org/>
[Pip]: <https://pip.pypa.io/en/stable/>
[cookiecutter]: <https://github.com/cookiecutter/cookiecutter>
[Read the Docs]: <https://readthedocs.org/>
[GitHub]: <https://github.com>
[GitLab]: <https://gitlab.com>
[Heptapod]: <https://foss.heptapod.net>
[GitLab Community Edition]: <https://gitlab.com/rluna-gitlab/gitlab-ce>
[AWS]: <https://aws.amazon.com/>
[Sphinx]: <https://www.sphinx-doc.org>
[Heptapod Community Edition]: <https://heptapod.net/pages/get-heptapod.html#get-heptapod>
[CoCalc]: <https://cocalc.com/>
[Jupytext]: <https://jupytext.readthedocs.io/en/latest/>
[The Executable Books Project]: <https://executablebooks.org/en/latest/>
[Jupyter Book]: <https://jupyterbook.org>
[Jupyter Book with Sphinx]: <https://jupyterbook.org/sphinx/index.html>
[Jupyter]: <https://jupyter.org>
[MyST]: <https://myst-parser.readthedocs.io/en/latest>
[MyST Cheatsheet]: <https://jupyterbook.org/reference/cheatsheet.html>
[Markdown]: <https://daringfireball.net/projects/markdown/>
[Perusall]: <https://app.perusall.com/>
[CommonMark]: <https://commonmark.org>
[reStructuredText]: <https://docutils.sourceforge.io/rst.html>
[MyST substitutions]: <https://myst-parser.readthedocs.io/en/latest/syntax/optional.html#substitutions-with-jinja2>
[Manim Community]: <https://www.manim.community/>
[sphinx-autobuild]: <https://github.com/executablebooks/sphinx-autobuild>
[genbadge]: <https://smarie.github.io/python-genbadge>
[shields.io]: <https://shields.io>
[sphinx-jupyterbook-latex]: <https://sphinx-jupyterbook-latex.readthedocs.io/en/latest/>
[PDF book using LaTeX]: <https://jupyterbook.org/en/stable/advanced/pdf.html#build-a-pdf-using-latex>
[texlive]: <https://www.tug.org/texlive/>
[ImageMagick]: <https://www.imagemagick.org/script/index.php>
[ReadTheDocs]: <https://readthedocs.org/>

<!-- global markdown links end -->

<!-- local markdown links -->
<!-- These will override the global links -->
["we're very commited to continuing support for mercurial"]: <https://community.atlassian.com/t5/Bitbucket-articles/What-to-do-with-your-Mercurial-repos-when-Bitbucket-sunsets/ba-p/1155380/page/8#M345>
[Bitbucket import]: <https://heptapod.net/pages/tuto-bb-import.html>
[abandoned the mercurial community]: <https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket>
[an issue]: <https://hg.iscimath.org/forbes-group/cookiecutter-templates/-/issues>
[escape]: <https://jinja.palletsprojects.com/en/latest/templates/#escaping>
[Mercurial]: <https://www.mercurial-scm.org/>
[Git]: <https://git-scm.com/>
[Hg-Git]: <https://hg-git.github.io/>
[black]: <https://black.readthedocs.io/en/stable/>
[jupyter_nbextensions_configurator]: <https://github.com/Jupyter-contrib/jupyter_nbextensions_configurator>

[issue 1636]: <https://github.com/cookiecutter/cookiecutter/issues/1636>
[issue 1176]: <https://github.com/cookiecutter/cookiecutter/issues/1176>
[pdm #1763]: <https://github.com/pdm-project/pdm/issues/1763>
[pdm #1758]: <https://github.com/pdm-project/pdm/discussions/1758>
[pdm #46]: <https://github.com/pdm-project/pdm/issues/46>
