# To Do

![Open Issues](https://img.shields.io/gitlab/issues/open/forbes-group%2Fcookiecutters)

* Add some sort of flag for WIP projects which default to EDITABLE=true.
* Consider including downgraded jupyter server...
* Add tool for offline mathjax

* [ ] [Issue #1][] with `hg-update-cookiecutter`: This fails quite badly if there are file renames, or if
  there are unmanaged (but not ignored) files.  Specifically, file renames are not
  detected and unmanaged files are added.  To demonstrate, try changing the project name
  or slug.  Both the old and the new source directories will appear.
  
  Some options:
  1. Do update in a clean temporary directory.
  2. Try to use `shelve` or similar to backup everything in the current directory, then
     clean and restore.
  
  I think the first is cleaner so am trying that first.  One issue I ran into is that
  cloning into a new directory does not preserve the phases, so instead, I just copy the
  `.hg` directory as [suggested here](https://stackoverflow.com/a/70879176/1088938).
  Alternatively, one might just symlink the `.hg` folder.

* `use_rosetta` option for Mac OS X on ARM platforms.  Might be needed if `use_manim`,
  but should be optional.
* I ran into some issues installing `pipx`.  (Minor issue if users want to install
  global tools only.)
  * `python3 -m pip install --user pipx` failed because my PATH served up a system
    (MacPorts) version of python that did not have [pip][]. It is probably fine to
    require that users setup a version of python with [pip][], or we could try running
    `pip install` instead.  This might find another python version, but might not put
    `pipx` on the path. If the `python3 -m pip` version works, we should be able to run
    `python3 -m pipx ensurepath` or something similar.
    
    Is there a simple and robust way to modify the PATH to find installed components?
    The tricky part is that this happens within a rule.  I think the solution would be
    to use [eval](https://www.gnu.org/software/make/manual/make.html#Eval-Function), but
    it would be better to avoid this.  *Try to make sense of this:*
    
    ```makefile
    a: b
		echo $(TMP)
    b:
		$(eval TMP := hi b)
    c:
		$(eval TMP := hi c)
    .PHONY: a b c
    ```


* Make sure that the kernel sets the appropriate path so that using that kernel has
  access to the source code in that repo even if `mmf_setup.set_path()` does not find
  the folder properly.

# Learn Scientific Python

The [Learn Scientific Python](https://learn.scientific-python.org) has similar aims, but
more generalized (i.e. supporting many backends).  We should synchronize.

# Work Log

## 20 Feb 2024

A comment on `make shell`: The idea here is to start a bash shell running in the new
environment.  We want this shell to be functional, so we invoke with `bash --init-file
.init-file.bash` where this file has useful customizations.  On Linux, we ran into some
problems related to the following issues:

* https://www.mail-archive.com/bug-bash@gnu.org/msg20804.html
* https://bugs.launchpad.net/ubuntu/+source/bash/+bug/589496

Basically, bash is compiled on Debian such that `--init-file` still loads the system
files, and there is no way to prevent since `--norc --no-profile` also negates
`--init-file`.  To work around this, we will use a custom variable `CUSTOM_CONDA_PREFIX`.

## 18 Jan 2024

Updating for a non-course project (hypersonic).

To do:
* Use new [boolean variables][].  CAREFUL! Needs testing:
  https://github.com/cookiecutter/cookiecutter/issues/1789
* Use the slugify [extension][].
* Find some way to raise an error if a user has not specified a critical variables like
  `project_name` (which will not be specified if `use_course == "no"`).  Maybe with a
  hook?  See `pre_gen_project.py`.

[boolean variables]: <https://cookiecutter.readthedocs.io/en/stable/advanced/boolean_variables.html>
[extension]: <https://cookiecutter.readthedocs.io/en/stable/advanced/template_extensions.html>

## 7 Jan 2023

Added a couple of options `notebook` and `performance` allowing users to add additional
dependencies through the config.  To get things working on RTD, the `notebook`
dependencies are only installed with `pip install .[notebook]` or `pip install .[all]`,
which is done locally with `make shell` etc. but not on RTD which just uses
`environment.yaml`.  This emphasizes that we probably need to have a minimal RTD
install, and use the `Makefile` to do more complete installs for the user.

## 6 Jan 2023
Starting a new course: "Mathematics 583: Analysis of and on Rough Sets".

* Kevin created the [Discourse
  topic](https://discourse.iscimath.org/t/mathematics-583-analysis-of-and-on-rough-sets/967). This
  is a good place to collect information such as:
  
  I pinned this topic globally so it appears at the top.
  
  * Title, Textbooks, Times, Location, Zoom id, Prerequisites.

  I gathered information from:
  
  * https://schedules.wsu.edu/:


* Created the following projects:
  * From [CoCalc: WSU Courses][] I created a course
    `Archive/2024-iSciMath-583-Spring.course` and the associated [shared
    project](https://cocalc.com/projects/0ac282a7-6965-436a-bf84-157dd1f7af9f/files/)

* I am installing from my local working copy of this project so:

  ```bash
  cd ~/current/courses
  hg clone git@gitlab.com:wsu-courses/iscimath-583-fractals.git iSciMath583_Fractals
  cd iSciMath583_Fractals
  cp ~/current/courses/521/Course/.cookiecutter.yaml .
  emacs .cookiecutter.yaml
  cookiecutter ~/current/cookiecutters/ --directory project \
               --config-file .cookiecutter.yaml --overwrite-if-exists --no-input
  hg add .cookiecutter.yaml
  hg com -m "Initial commit of .cookiecutter.yaml config file."
  hg branch cookiecutter-base
  hg add
  hg com -m "BASE: Initial cookiecutter template"
  hg up default
  hg merge cookiecutter-base
  hg com -m "Merge in cookiecutter updates"
  hg bookmark main # If using hg-git
  ```
  
* Added the CoCalc Shared project to my `~/.ssh/config` file as `smc583`:
* Clone repo there.

  ```bash
  ssh smc583
  mkdir .repositories
  git clone git@gitlab.com:wsu-courses/iscimath-583-fractals.git
  cd iscimath-583-fractals
  make init
  ```

* On `smcwsu` and some other CoCalc projects, I needed to add `evolve`:

  ```bash
  ssh smcwsu
  head "$(type -p hg)" -n 1   # Gives /usr/bin/python3
  /usr/bin/python3 -m pip install --user hg-evolve
  ```

* Add mercurial repo on `smcwsu`.  Locally I add `wsu =
  ssh://smcwsu/.repositories/iSciMath-583-Fractals` to the `[paths]` section of `.hg/hgrc`.

  ```bash
  ssh smcwsu
  cd .repositories
  mkdir iSciMath-583-Fractals
  cd iSciMath-583-Fractals
  hg init
  cat <<EOF >> .hg/hgrc 
  [phases]
  publish = False
  EOF
  ```
  
  Then I can do `hg push wsu -r .` on my local computer and `hg up` on `smcwsu`.
  
* Issues with `make init` on CoCalc:
  * I was first installing `micromamba` into `envs/tools/bin` because I wanted to
    distribute, but then, when trying to make this environment I get the following error
    ```
    error    libmamba Non-conda folder exists at prefix
    ```
  

## 13 Nov 2023

* Updating `pytimeode` to use this project with `use_pyproject: "pdm"` and `use_conda:
  "no"`.  This will be an example of how to manage everything with PDM.  The main issue
  is having our `Makefile` create a proper environment without using any sort of
  `conda`.  In this case, we assume that the user has installed an appropriate version
  of python on their system.  For details see [PDM: Working with Virtual
  Environments](https://pdm-project.org/latest/usage/venv/).
  
  Before doing this, we need to figure out how to ensure we have `pipx` etc.  If
  `USER_INSTALL_OK = true`, then this will be installed globally.
  
  
  
  
## 27 Aug 2023

* Getting a base version working for my courses (521 Classical Mechanics and 581 The
  Standard Model).  I had to fix the following:
  * Added `install_tools`.  This is currently not properly used (only in
    `anaconda-project.yaml`).  We should probably just provide a Makefile target, but
    maybe we strip it out of this is "no".
  * Remove `use_pdm`.  We will not use `use_pyproject = "pdm"` for this option.
  * Ran into [issue 1892][].  We will have to rely on the defaults generated for now.
  * After cleaning up a bunch of things, I could not `make doc-server` out of the box.
    Getting this to run now...
  * Cleaned up some things to get a pure miniconda environment working:
    * Cleanup unused `environment.yaml`, `anaconda-project.yaml`, `pyproject.toml`
      files.

## 1 Sep 2023

* Updated `Makefile` to register kernel.

# Cookiecutter Limitations

[Cookiecutter][] is currently the best tool I know of, but it has several limitations to
keep in mind.

* If you need a specific version, you can install it with git.  *(This was an issue when
  versions were not yet available on PyPI.)*

  ```bash
  pip install --upgrade --user git+https://github.com/cookiecutter/cookiecutter.git@2.0.2#egg=cookiecutter=2.0.2
  ```

* The config formats ([JSON][] for `cookiecutter.json`, [YAML][] for replay files) are quite
  limiting, including:
  
  * [JSON][] does not support comments (see [issue 970][]).
  
* No clear solution for updating templates.  We do it by maintaining a
  `cookiecutter-base` branch to which we pull any template updates, then use the VCS
  merge tools to merge these forward.  This results in minimal merge problems -- only
  places where both the template and the code have changed lines.

  * [issue 784][] (closed): How to update a project with changes from its cookiecutter?
    Here someone recommended [cruft][], but I had problems.  (Not sure why right now.
    Might have been install issues.)
  * [scaraplate][] looks interesting.  Restricted to git though.

* Since [Jinja][] is used, if you use Jinja templating in your project, you get a somewhat
  messy templating nesting issue.  Some people have requested changes to help (see [issue
  1624][]) but ultimately the solution is to use `{% raw %}` with careful attention to
  [whitespace
  control](https://jinja.palletsprojects.com/en/3.0.x/templates/#whitespace-control).
  One user has a [nice
  suggestion](https://github.com/cookiecutter/cookiecutter/issues/1624#issuecomment-1076475537)
  of commenting these so that your linters can still fuction:
  
  ```yaml
  #{%- raw -%}
  name: MyWorkflow
  ...
  #{%- endraw -%}
  ```

* Update with `--overwrite-if-exists` sometimes fails if certain directories exist
  [issue 1176][].  There are [some PRs that fix
  this](https://github.com/cookiecutter/cookiecutter/issues/1176#issuecomment-546199966),
  but the maintainers have [not had  the bandwidth to apply
  these](https://github.com/cookiecutter/cookiecutter/pull/1030#issuecomment-367401690).
  
  To deal with this, just remove the offending folders before updating.

* There is no direct way to deal with conditional files.  This must be done with
  [hooks](https://cookiecutter.readthedocs.io/en/2.0.2/advanced/hooks.html?highlight=conditional#example-conditional-files-directories)
  that run after.
   
* Cookiecutter does not support symlinks.  We [manually create them with a post gen
  hoo](https://github.com/cookiecutter/cookiecutter/issues/865#issuecomment-1019760635)
  but this currently needs them to be specified in the config.  See [issue 865][], [issue
  1420][], and references therein.

* Cookiecutter stores information in `~/.cookicutters` and `~/.cookiecutter_replay`
  without any way to customize this. (See [issue 104][] and [issue 1417][]).  Some
  manipulation might be needed.

* [Issue 1892][]: By default, lists in `cookiecutter.json` are used to represent
  options.  This precludes the use of lists unless they are nested in a [dict
  variable](https://cookiecutter.readthedocs.io/en/stable/advanced/dict_variables.html).
  Unfortunately, in version 2.2.0, if you try to overwrite these values in your local
  `.cookiecutter.yaml` file, then it will trigger the check for options, raising an
  error and killing your build.

## Relevant Issues

*(Ordered by descending ticket number -- newest first.)*

* [Support lists that are not options][issue 1892]: 
* <s>[Cookiecutter maintenance - consider joining collaborative community like
  Jazzband][issue 1642]</s> (closed)
* <s>[2.0.1 and 2.0.2 are not available on PyPI][issue 1636]</s> (closed)
* [cookiecutter should only process values starting with {{cookiecutter.}}][issue 1624]
* <s>[2.0.1][issue 1555]</s> (closed)
* <s>[Help maintaining cookiecutter?][issue 1536]</s> (closed)
* [Documentation 2.0: Describe symlinks][issue 1420]
* [Adding an option to generate a .cookiecutter_replay inside of generated repos][issue 1417]
* [FileExistsError even though --overwrite-if-exists is specified.][issue 1176]
* [Use YAML for cookiecutter configuration][issue 970]:
* [Symlinks become normal directories when cookiecutter is run][issue 865]
* [How to update a project with changes from its cookiecutter?][issue 784] (closed) 
* [New config file format][issue 249]
* <s>[Support alternate config location as per XDG Base Directory Spec][issue 104]</s> (closed)

# Implementation Details

## Tools

We assume that the platform has the following tools installed and provide no resources
to install these ourselves.  Most Linux systems, Mac OS X, and [Windows Subsystem for Linux][WSL]
([WSL][]) provide these, or make them easy to get.

* **[bash][]**: [GNU bash][bash] shell.  We try to make everything shell agnostic
  referring to the `SHELL` variable, but may need explicit support for different shells
  (e.g. analogs of the `.init-file.bash` file).
* **[find][]** etc.: Used for cleaning.
* **[make][]**: [GNU Make][make] for using the `Makefile`.
* **[curl][]**: command line tool and library for transferring data with URLs.

### Local Tools 

The following tools are also needed, but we provide provisions for installing them if
`INSTALL_TOOLS_OK=true` (default).  Where should these go?

* `envs/tools`: I like the idea of keeping everything installed in one place so that it
  is easy to clean.  A problem is that if we use `micromamba` to provision this
  environment, then we can't first install `micromamba` here.  (I tried doing this
  initially, but you then get `error libmamba Non-conda folder exists at prefix`.)  I
  think we will keep this, but symlink things into `.local/bin`.
* `.local/`: Mirrors `~/.local/`.  We will then have to symlink everything in here as
  needed.  This is a natural place for things like `micromamba`




These are installed either in special virtual
environments, and linked to `LOCAL=.local`.  This behavior can be overridden as follows:

1. `USER_INSTALL_OK=true`.  This will not install `python3`, and will install everything
   else globally using the `--user` flag for example.  This is useful on [CoCalc][] for
   example where these tools are shared.  Note: `make realclean` will not remove these.
2. If `.tools/environment.tools.yaml` exists, then `micromamba` will be used to create
   `LOCAL` if needed using this file to provision.
3. Otherwise, if `LOCAL_INSTALLER=micromamba` (default), then `micromamba` will be used to create
   `LOCAL` as a bare environment.
4. Otherwise, we require `python3` and will create `LOCAL` as a venv.


* **python3**: To install these tools, some version of Python is required.  If this does
  not exist, then `micromamba` is used to install one.
* **[pipx][]**: Installs and runs python applications in isolated environments.
  *To do: consider installing [pipx][] using [micromamba][] if not found.*
* **[conda][]**: If `use_conda == "conda"`, then this is needed.
 *To do: consider installing [conda][] using [micromamba][] if not found.*
* **[pdm][]**: If `use_pyproject == "pdm"`, then this is needed.
 *To do: consider installing [pdm][] using [pipx][] if not found.*

To function, a certain set of tools is needed.  Some of these -- like [Sphinx][] -- will
be installed with the project (see Provisioning below).  Others should be installed
externally.  Generally, these tools should be provided by the system, and our default
behaviour is to just expect them to be in place.  However, as a convenience to users, we
provide a `make tools` target that will install the necessary tools.  The following
special tools are coded in the `Makefile`:

* **[micromamba][]**: This will be installed using [curl][] as [recommended](https://mamba.readthedocs.io/en/latest/installation.html#install-script):
  
  ```bash
  curl -L micro.mamba.pm/install.sh | "${SHELL}"
  ```
  
  This will install [micromamba][] in `~/.local/bin` and needs some modification for a
  local install in the project. (We do this with some simple replacements.)

* **[jupytext][]**: Needed for `make sync`.  Installed with [pipx][] if needed.


Here are some options:

* Use a [Conda][] `environment.tools.yaml` file.  We simply create the appropriate
  `envs/tools/` environment and then add `envs/tools/bin/` to `PATH`.
  *Relevant `Makefile` variables are*
    * `TOOLS_ENV = envs/tools/`
    * `TOOLS_ENV_FILE = .tools/environment.tools.yaml`
* Use [pipx][] and [condax][] to install the necessary tools.  [Pipx][] works well in
  general, but [condax][] has some issues:
  * [issue #63](https://github.com/mariusvniekerk/condax/issues/63): Can't specify the
    version of python.
  * [issue #16](https://github.com/mariusvniekerk/condax/issues/16): Can't easily
    specify the location of the install (`~/.local/bin` is somewhat hardcoded).

  We generally do not want to hard-code these tools in `Makefile`, so for this approach,
  we use files like `.tools/requirements.jupytex.txt` coupled with the suggestions in
  [issue #359](https://github.com/pypa/pipx/issues/359) to inject dependencies with
  something like 
  
  ```bash
  cat requirements.txt | sed -e 's/#.*//' | xargs pipx inject <package>.
  ```
  
  We should look into [mambax][].  Still WIP though.
  
* Use development dependencies of [PDM][] or [Poetry][].  There are currently no special
  provisions for this in the `Makefile`.
  *Relevant `Makefile` variables are*
    * `TOOLS_ENV = envs/tools/`
    * `TOOLS_ENV_FILE = .tools/environment.tools.yaml`

[mambax]: <https://github.com/maresb/mambax>

## Makefile

We follow a couple of conventions in our `Makefile`.

* We define the executable for various commands in variables like `CONDA_EXE`,
  `MICROMAMBA_EXE` etc.  These are also used as prerequisites to define the method of
  installation (see Tools above).  To run the command, we often need additional
  environmental variables defined, which is done in `CONDA_PRE`, `MICROMAMBA_PRE`, etc.
  Finally, all of this is packaged into the command name `CONDA` along with any
  arguments in `CONDA_FLAGS` etc.
  
  Note: in some cases, such as with `find` or `xargs`, one needs to separate the `*_PRE`
  component with a simple invocation.  Previously we dealt with this by setting the
  variables before calling the function, but this requires separating everything.  Now
  we fix this by [using a sub-shell](https://unix.stackexchange.com/a/244027/37813), but
  be aware of the security implications of passing `{}` directly to the subshell.  So,
  for example:
  
  ```bash
  find . -name "*.ipynb" -exec $(SHELL) -c \
      '$(JUPYTEXT) --sync "$$1"' \
    $(SHELL) {} +
  ```
  
  instead of 

  ```bash
  $(JUPYTEXT_PRE) find . -name "*.ipynb" -exec $(JUPYTEXT_EXE) --sync {} +
  ```

Here is a use-case for installing tools.  *(This example is in the `playground/make1`
folder.)*   I want to write

```Makefile
all: cowsay
	cowsay -t "Moo $*"
```

I want this to require `cowsay` to exist as a command, and to install it if it is does
not exist.  The challenge is that the target `cowsay` does not exist. 

1. Here is something that works [suggested on stack overflow](
   https://stackoverflow.com/a/23136180/1088938):

   ```Makefile
   VER = 1
   LOCAL = envs/tools
   BIN = $(LOCAL)/bin
   cowsay = $(BIN)/cowsay

   all: $(cowsay)
   	$(cowsay) -t "Moo $* VER=$(VER)"

   $(BIN)/%:
   	PIPX_HOME=envs/pipx PIPX_BIN_DIR=$(dir $@) pipx install $*
   ```

   This is nice and explicit, but has a few issues:
   
   * It fails if `pipx` is not installed (we will fix this later).
   * It always installs a local version of `cowsay`.  It would be nice to be able to
     use a global version of `cowsay` if it exists, and only install a local version if
     needed.
   
2. Here we check if a global version exists.  We also add the install location to `PATH`
   so we can call `cowsay` directly.

   ```Makefile
   VER = 2
   LOCAL = envs/tools
   BIN = $(LOCAL)/bin
   PATH := $(BIN):$(PATH)

   all: cowsay
   	cowsay -t "Moo $* VER=$(VER)"

   $(BIN)/%:
   	PIPX_HOME=envs/pipx PIPX_BIN_DIR=$(dir $@) pipx install $*

   cowsay:
   	@command -v $@ || make $(BIN)/$@

   .PHONY: cowsay
   ```

3. This works nicely, but can we make the checking rule implicit?  This seems to work too:

   ```Makefile
   VER = 3
   LOCAL = envs/tools
   BIN = $(LOCAL)/bin
   export PATH := $(BIN):$(PATH)

   all: cowsay
   	cowsay -t "Moo $* VER=$(VER)"

   $(BIN)/%:
   	PIPX_HOME=envs/pipx PIPX_BIN_DIR=$(dir $@) pipx install $*

   %:
   	@command -v $@ || make $(BIN)/$@
   ```

4. Going a little further, we want to inject requirements.

   ```Makefile
   VER = 4
   LOCAL = envs/tools
   BIN = $(LOCAL)/bin
   PATH := $(BIN):$(PATH)

   all: cowsay
   	cowsay -t "Moo-$$(cowsay --version) $* VER=$(VER)"

   PIPX = PIPX_HOME=envs/pipx PIPX_BIN_DIR=$(dir $@) pipx

   $(BIN)/%: .tools/requirements.%.txt
   	$(PIPX) install $*
   	$(PIPX) inject $* $(shell cat $<)
   	touch $@

   $(BIN)/%:
   	$(PIPX) install $*

   %:
   	@( ! command -v $@ || \
   	  [ "$$(command -v $@)" -ef "$$(command -v $(BIN)/$@)" ] \
   	) && $(MAKE) $(BIN)/$@ || true
   ```

   We also modify the `command -v` test to solve the following problem. If we touch
   `.tools/requirements.cowsay.txt`, the target will not be rebuilt because the simple
   rule simplify finds the installed command and then does not trigger the sub-make.
   If the command is defined, we further check to see if it [is identical](
   https://stackoverflow.com/a/67948982/1088938) to the version in `$(BIN)`.
   
   We also `touch` the command since it might not be reinstalled if only the injected
   requirements change.

5. What if we don't want to install the needed programs, but just want to tell the user
   they should install them?
   
   ```Makefile
   VER = 5
   LOCAL = envs/tools
   BIN = $(LOCAL)/bin
   PATH := $(BIN):$(PATH)

   all: cowsay
   	cowsay -t "Moo-$$(cowsay --version) $* VER=$(VER)"

   PIPX = PIPX_HOME=envs/pipx PIPX_BIN_DIR=$(dir $@) pipx

   $(BIN)/%: .tools/requirements.%.txt
   	$(PIPX) install $*
   	$(PIPX) inject $* $(shell cat $<)
   	touch $@

   $(BIN)/%:
   	$(PIPX) install $*

   %:
   	@command -v $@ || \
   	(echo "I do not know how to make \"$@\". Can you install it (e.g. with apt-get)?" && false)
   ```

6. This all works quite well, but there is now one gotcha.  I also want to install
   `pipx` if it does not exist:
   
   ```Makefile
   VER = 6
   LOCAL = envs/tools
   BIN = $(LOCAL)/bin
   PATH := $(BIN):$(PATH)

   all: cowsay
   	cowsay -t "Moo-$$(cowsay --version) $* VER=$(VER)"

   PIPX = PIPX_HOME=envs/pipx PIPX_BIN_DIR=$(dir $@) pipx

   $(BIN)/%: .tools/requirements.%.txt pipx
   	$(PIPX) install $*
   	$(PIPX) inject $* $(shell cat $<)
   	touch $@

   $(BIN)/%: pipx
   	$(PIPX) install $*

   pipx: python3
   	@command -v $@ || python3 -m pip install --user pipx

   %:
   	@( ! command -v $@ || \
   	[ "$$(command -v $@)" -ef "$$(command -v $(BIN)/$@)" ] \
   	) && $(MAKE) $(BIN)/$@ || true

   pipx: python3
   	@command -v $@ || ( python3 -m ensurepip --user --upgrade && \
   	                    python3 -m pip install --user pipx )
   .NOTINTERMEDATE: pipx
   ```

   What is not obvious (to me at least) is that we need something like 
   `.NOTINTERMEDATE: pipx`.  There are subtleties with how [pattern rules work](
   https://www.gnu.org/software/make/manual/html_node/Pattern-Match.html):

   > A pattern rule can be used to build a given file only if there is a target pattern
   > that matches the file name, and all prerequisites in that rule either exist or can be
   > built.

   This got me when I tried to include some catch-all rules:

   ```makefile
   LOCAL = envs/tools
   BIN = $(LOCAL)/bin
   PIPX = PIPX_HOME=envs/ PIPX_BIN_DIR=$(dir $@) pipx

   %:
       @command -v $@ || \
       (echo "I do not know how to make \"$@\". Can you install it (e.g. with apt-get)?" && false)

   $(BIN)/%: pipx
       $(PIPX) install $*

   cowsay:
       @command -v $@ || make $(BIN)/cowsay

   .PHONY: cowsay
   ```

   This gives the following errors:

   ```bash
   $ make pipx  # This is good
   I do not know how to make "pipx". Can you install it (e.g. with apt-get)?
   make: *** [pipx] Error 1
   $ make cowsay  # Huh?  Why does this not give the same error?
   make[1]: *** No rule to make target `.local/bin/cowsay'.  Stop.
   make: *** [cowsay] Error 2
   ```

   Apparently the rule `cowsay` matches, but the pattern rule `$(BIN)/%` does
   not because the prerequisite `pipx` does not exist and apparently cannot be built.  I
   did non fully understand, since there is a rule for `pipx` that matches (and in
   principle, which could make `pipx`), but [later, we find](
   https://www.gnu.org/software/make/manual/html_node/Match_002dAnything-Rules.html)

   > A non-terminal match-anything rule cannot apply to a prerequisite of an implicit
   > rule

   Making the match-anything rule terminal `%::` gives

   ```bash
   $ make cowsay  # Huh?  Why does this not give the same error?
   I do not know how to make ".local/bin/cowsay". Can you install it (e.g. with apt-get)?
   make[1]: *** [.local/bin/cowsay] Error 1
   make: *** [cowsay] Error 2
   ```

   This is a little more tricky... Now I am not sure why this rule has precedence.  A
   [hint is given by Beta on Stack
   Overflow](https://stackoverflow.com/a/3081722/1088938): *"Only if it fails to find any
   rule whose prerequisites exist will it resort to looking for rules to build the
   prerequisites."*  This implies that our rule with `$(BIN)/%: pipx` is skipped at first
   to see if there is a rule without a prerequisite, which there is. This is hard to
   find, but is in the [Implicit Rule Search Algorithm](
   https://www.gnu.org/software/make/manual/make.html#Implicit-Rule-Search) step 5:

   > Test whether all the prerequisites exist or ought to exist. (If a file name is
   > mentioned in the makefile as a target or as an explicit prerequisite of target T, then
   > we say it ought to exist.) 
   >
   > If all prerequisites exist or ought to exist, or there are no prerequisites, then
   > this rule applies.

   Several things can be done, though I am not exactly sure how these work... I think by
   changing the meaning of "ought to exist" above:

   1. Include `pipx:` as an empty rule.
   2. Specify `.NOTINTERMEDIATE: pipx`.


Another issue 

## Provisioning

For provisioning, we use a combination of some sort of [Conda][], optionally with
another tool like [PDM][] or [Poetry][] to manage pure Python dependencies.  The
[Conda][] tool allows one to install things like [CuPy][] which require non-python
dependencies such as [CUDA][].  These are customized by the following flags:

* `use_conda`: Use some sort of [Conda][] to setup the base environment.  Takes one of
  the following values:
  * `"anaconda_project"`: Used to be the default, but we are moving away from this. This
    uses an `anaconda_project.yaml` file to specify the dependencies.  [Anaconda
    Project][] also has a CLI for adding dependencies etc., but this does not always
    work preserving the form of the `anaconda_project.yaml` file for use as an
    `environment.yaml` file (which is generally needed by [Read the Docs][] for example).
  * `"conda"`: Use a pure [Conda][] `environment.yaml` file to provision the environment.
  * `"micromamba"`: Like `"conda"`, but use the [Micromamba][] tool instead.
  * `"no"`: Do not use a [Conda][]-like tool.
* `use_pyproject`: Use some sort of tool to manage `pyproject.toml` for specifying pure
  Python dependencies.  This can take one of the following values:
  * `"pdm"`: Use [PDM][] to manage the `pyproject.toml` file.
  * `"poetry"`: Use [Poetry][] to manage the `pyproject.toml` file.
  * `"pip"`: Do not specify a tool to manage the `pyproject.toml` file, but use [pip][]
    to install pure python dependencies.
  * `"no"`: Do not use a `pyproject.toml` file.

### CoCalc

We detect if we are on a CoCalc platform by looking for the presence of the environment
variable `ANACONDA2020`.  *Note: this means you must provision the environment with
this on CoCalc Docker images since it is not there by default.  We do this with the
following `Dockerfile`:*

<details><summary>Dockerfile for CoCalc</summary>

```
# Dockerfile for our CoCalc Images (as of July 2023)
FROM sagemathinc/cocalc:latest

MAINTAINER Michael McNeil Forbes <michael.forbes+python@gmail.com>

USER root

# Anaconda
ARG ANACONDA_VER=2022.05
ARG EXT="/opt"
ARG ANACONDA="${EXT}/anaconda${ANACONDA_VER}"
RUN cd /tmp \
 && export ARCH2=`uname -m` \
 && mkdir -p "${EXT}" \
 && wget -q "https://repo.anaconda.com/archive/Anaconda3-${ANACONDA_VER}-Linux-${ARCH2}.sh" \
 && bash "Anaconda3-${ANACONDA_VER}-Linux-${ARCH2}.sh" -b -p "${ANACONDA}" \
 && ${ANACONDA}/bin/conda create -n mamba -c conda-forge mamba \
 && "${ANACONDA}/bin/python3" -m ipykernel install --name "anaconda${ANACONDA_VER}" --display-name "Python 3 (Anaconda 2022)" \
 && wget -q "https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh" \
 && bash Mambaforge-$(uname)-$(uname -m).sh -b \
 && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
       tree \
       ncdu \
       python3-venv\
       rclone texlive-full \
       ffmpeg \
    && rm -rf /var/lib/apt/lists/* \
 && python3 -m pip install --no-input --upgrade pip pipx \
 && export PIPX_BIN_DIR=/usr/local/bin \
 && pipx install mercurial \
 && pipx inject mercurial hg-evolve hg-git \
 && pipx install pdm \
 && pipx install poetry \
 && echo "[ -r /etc/bash-mmf-smc.bashrc ] && . /etc/bash-mmf-smc.bashrc" >> /etc/bash.bashrc

COPY ./bash-mmf-smc.bashrc /etc/bash-mmf-smc.bashrc

# Added miniconda, and provision the base environment using our mforbes/base
# environment on anaconda-cloud
# RUN \
#     mkdir /zips \
#  && wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
#  && bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda \
#  && ln -s /opt/conda/bin/conda /bin/conda \
#  && conda install -y -n base anaconda-client \
#  && conda env update -n base mforbes/base
```
</details>

[CoCalc]: <https://cocalc.com>
[CoCalc: WSU Courses]: <https://cocalc.com/projects/c31d20a3-b0af-4bf7-a951-aa93a64395f6>
[Cookiecutter Discord]: <https://discord.gg/hhBhGuBTqY>
[ac3486]: <https://github.com/ansible-community/molecule/issues/3486>
[issue 249]: <https://github.com/cookiecutter/cookiecutter/issues/249>
[issue 104]: <https://github.com/cookiecutter/cookiecutter/issues/104>
[issue 1536]: <https://github.com/cookiecutter/cookiecutter/issues/1536>
[issue 1555]: <https://github.com/cookiecutter/cookiecutter/issues/1555>
[issue 1636]: <https://github.com/cookiecutter/cookiecutter/issues/1636>
[issue 970]: <https://github.com/cookiecutter/cookiecutter/issues/970>
[issue 784]: <https://github.com/cookiecutter/cookiecutter/issues/784>
[issue 1642]: <https://github.com/cookiecutter/cookiecutter/issues/1642>
[issue 1624]: <https://github.com/cookiecutter/cookiecutter/issues/1624>
[issue 1176]: <https://github.com/cookiecutter/cookiecutter/issues/1176>
[issue 865]: <https://github.com/cookiecutter/cookiecutter/issues/865>
[issue 1420]: <https://github.com/cookiecutter/cookiecutter/issues/1420>
[issue 1417]: <https://github.com/cookiecutter/cookiecutter/issues/1417>
[issue 1892]: <https://github.com/cookiecutter/cookiecutter/issues/1892>

[Anaconda Project]: <https://github.com/Anaconda-Platform/anaconda-project> "Anaconda Project"
[CUDA]: <https://developer.nvidia.com/cuda-toolkit> "CUDA Toolkit"
[Conda]: <https://docs.conda.io>
[CuPy]: <https://cupy.dev> "CuPy: NumPy/SciPy-compatible Array Library for GPU-accelerated Computing with Python"
[JSON]: <https://en.wikipedia.org/wiki/JSON>
[Read the Docs]: <https://readthedocs.org> "Read the Docs homepage"
[YAML]: <https://en.wikipedia.org/wiki/YAML>
[cookiecutter docs]: <https://cookiecutter.readthedocs.io/>
[cookiecutter]: <https://github.com/cookiecutter/cookiecutter>
[cruft]: <https://cruft.github.io/cruft/>
[micromamba]: <https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html>
[scaraplate]: <https://scaraplate.readthedocs.io/en/latest/>
[pip]: <https://pip.pypa.io> "pip: the package installer for Python"
[PDM]: <https://pdm.fming.dev/latest/>
[Poetry]: <https://pdm.fming.dev/> "Python packaging and dependency management made easy."
[Sphinx]: <https://www.sphinx-doc.org/>
[pipx]: <https://pypa.github.io/pipx/>
  "Install and Run Python Applications in Isolated Environments"
[condax]: <https://github.com/mariusvniekerk/condax>
[micromamba]: <https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html>
[make]: <https://www.gnu.org/software/make/>
[bash]: <https://www.gnu.org/software/bash/>
[curl]: <https://curl.se> "command line tool and library for transferring data with URLs"
[WSL]: <https://learn.microsoft.com/en-us/windows/wsl/>
[Jupytext]: <https://jupytext.readthedocs.io>
  "Jupyter Notebooks as Markdown Documents, Julia, Python or R Scripts"
[find]: <https://www.gnu.org/software/findutils/manual/html_mono/find.html> "GNU Findutils"
[Jinja]: <https://jinja.palletsprojects.com/en/3.1.x/>
[Issue #1]: <https://gitlab.com/forbes-group/cookiecutters/-/issues/1>


