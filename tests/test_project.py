from fnmatch import fnmatch
from functools import wraps
import os
import subprocess
from pathlib import Path
import random

from cookiecutter.main import cookiecutter

import yaml

import pytest


# These will override values in the copied config file
_DEFAULT_CONTEXT = dict(
    repository_url="",
)

# Scenarios for full testing
FULL_SCENARIOS = [
    dict(
        use_conda="micromamba",
        use_pyproject="pdm",
        use_docs="no",
        use_cocalc="no",
        use_course="no",
    ),
    dict(
        use_conda="micromamba",
        use_pyproject="pdm",
        use_docs="yes",
        use_cocalc="yes",
        use_course="no",
    ),
    dict(
        use_conda="micromamba",
        use_pyproject="pdm",
        use_docs="yes",
        use_cocalc="no",
        use_course="yes",
    ),
]


@pytest.fixture(params=["micromamba", "conda", "anaconda_project", "no"])
def use_conda(request):
    yield request.param


@pytest.fixture(params=["pdm", "poetry", "pip", "no"])
def use_pyproject(request):
    yield request.param


@pytest.fixture(params=["yes", "no"])
def use_docs(request):
    yield request.param


@pytest.fixture(params=["yes", "no"])
def use_cocalc(request):
    yield request.param


@pytest.fixture(params=["yes", "no"])
def use_test_assignments(request):
    yield request.param


@pytest.fixture(params=["yes", "no"])
def use_genbadge(request):
    yield request.param


@pytest.fixture(params=[0, 1])
def features(request):
    random.seed(2)
    keys = [
        "use_latex",
        #'use_binder',
        "use_test_assignments",
        #'use_experimental',
        "use_manim",
        "use_course",
    ]

    vals = ["yes", "no"]
    if request.param:
        vals = vals[::-1]
    yield {key: random.choice(vals) for key in keys}


@pytest.fixture
def config(
    use_conda,
    use_pyproject,
    features,
    use_genbadge,
    use_docs,
    use_cocalc,
):
    config = {}
    default_context = config.setdefault("default_context", {})
    default_context.update(_DEFAULT_CONTEXT)
    default_context["COOKIECUTTER_URL"] = str(Path.cwd())
    default_context["use_docs"] = use_docs
    default_context["use_cocalc"] = use_cocalc
    default_context["use_genbadge"] = use_genbadge
    default_context["use_conda"] = use_conda
    default_context.update(features)
    default_context["use_pyproject"] = use_pyproject
    default_context["title"] = "Test Project"

    if default_context["use_course"] == "no":
        # Need to provide one_line_description for non-courses
        default_context["one_line_description"] = "Testing cookiecutters"
    else:
        default_context["course_number"] = "581"
    yield config


def scenarios(f):
    """Decorator that filters config, skipping those not in FULL_SCENARIOS."""
    global FULL_SCENARIOS

    @wraps(f)
    def wrapper(*v, **kw):
        if "config" in kw and not any(
            all(
                kw["config"]["default_context"][key] == scenario[key]
                for key in scenario
            )
            for scenario in FULL_SCENARIOS
        ):
            pytest.skip("Config not in FULL_SCENARIOS")
        f(*v, **kw)

    return wrapper


class Result(object):
    """Holds the captured result of the cookiecutter project generation."""

    def __init__(self, exception=None, exit_code=0, project_dir=None, context=None):
        self.exception = exception
        self.exit_code = exit_code
        self.context = context
        self._project_dir = project_dir

    @property
    def project_path(self):
        """Return a Path object if no exception occurred."""

        if self.exception is None:
            return Path(self._project_dir)

        return None

    def __repr__(self):
        if self.exception:
            return "<Result {!r}>".format(self.exception)

        return "<Result {}>".format(self.project_path)


class Cookie:
    """Class representing a "baked" project directory (called the "cookie").

    Based on ideas in https://github.com/hackebrot/pytest-cookies
    """

    def __init__(self, project_dir, cookiecutters_dir, replay_dir):
        self.project_dir = project_dir
        self.cookiecutters_dir = cookiecutters_dir
        self.replay_dir = replay_dir
        self.config_file = project_dir / ".cookiecutter.yaml"

    def exists(self, path):
        """Return True if the path in the project directory exists."""
        return (self.project_dir / path).exists()

    def bake(self, config, template=Path("."), directory="project"):
        """Run cookiecutter and generate project."""
        config["replay_dir"] = str(self.replay_dir)
        config["cookiecutters_dir"] = str(self.cookiecutters_dir)
        self.config_file.write_text(yaml.dump(config))

        exception = None
        exit_code = 0
        project_dir = None
        context = None

        """
        context_file = pathlib.Path(template) / "cookiecutter.json"

        try:
            # Render the context, so that we can store it on the Result
            context = prompt_for_config(
                generate_context(
                    context_file=str(context_file), extra_context=extra_context
                ),
                no_input=True,
            )
        """

        context = None
        try:
            # Run cookiecutter to generate a new project
            project_dir = cookiecutter(
                template=str((template / directory).absolute()),
                no_input=True,
                output_dir=str(self.project_dir),
                overwrite_if_exists=True,
                config_file=str(self.config_file),
            )
        except SystemExit as e:
            if e.code != 0:
                exception = e
            exit_code = e.code
        except Exception as e:
            exception = e
            exit_code = -1

        return Result(
            exception=exception,
            exit_code=exit_code,
            project_dir=project_dir,
            context=context,
        )

    def update_config(self, config, **kw):
        """Update config.  (Used for testing changes.)"""
        config["default_context"].update(kw)
        self.config_file.write_text(yaml.dump(config))


@pytest.fixture
def cookie(tmp_path, capsys):
    # Where the project will be built.
    project_dir = tmp_path / "the_project"

    # Store downloaded cookiecutter templates here.
    cookiecutters_dir = tmp_path / "cookiecutters"

    # Store the replay here.
    replay_dir = tmp_path / "cookiecutter_replay"

    for _dir in [project_dir, cookiecutters_dir, replay_dir]:
        _dir.mkdir()

    with capsys.disabled():
        print(f"Baking cookies in {tmp_path}")

    yield Cookie(project_dir, cookiecutters_dir, replay_dir)


def test_bake_project(cookie, config):
    result = cookie.bake(
        config=config,
    )

    assert result.exit_code == 0
    assert result.exception is None

    c = config["default_context"]
    if c["use_docs"] == "yes":
        assert cookie.exists("Docs")
    else:
        assert not cookie.exists("Docs")

    if c["use_docs"] == "yes" and c["use_course"] == "yes":
        assert cookie.exists("Docs/Syllabus.md")
    else:
        assert not cookie.exists("Docs/Syllabus.md")

    res = subprocess.run(
        ["make", "help"],
        cwd=cookie.project_dir,
        capture_output=True,
        universal_newlines=True,
    )
    res.check_returncode()


#pytest.mark.skip("Making the projects is very slow and needs outcome testing.")
@scenarios
def test_make_project(cookie, config):
    result = cookie.bake(
        config=config,
    )
    print(result.project_path)

    assert result.exit_code == 0
    assert result.exception is None

    res = subprocess.run(
        ["make", "init"],
        cwd=cookie.project_dir,
        capture_output=True,
        universal_newlines=True,
    )

    assert not res.returncode

    if config["default_context"]["use_docs"] == "yes":
        res = subprocess.run(
            ["make", "html"],
            cwd=cookie.project_dir,
            capture_output=True,
            universal_newlines=True,
        )
        assert not res.returncode
    
    # assert result.project_path.name == "example-project"
    assert result.project_path.is_dir()


class Tests:
    """Specific test cases to make sure everything works as expected."""

    @staticmethod
    def fnmatch(string, expected):
        """Checks that string matches expected line-by-line.

        This allows for potential reordering.
        """
        string = string.strip()
        lines = expected.strip().splitlines()
        results = [fnmatch(string, f"*{line}*") for line in lines]
        return all(results)

    @scenarios
    def test_hg_update_cookiecutter_base(self, tmpdir, cookie, config):
        result = cookie.bake(
            config=config,
        )
        assert result.project_path
        res = subprocess.run(
            ["make", "hg-init"],
            cwd=cookie.project_dir,
            capture_output=True,
            universal_newlines=True,
        )
        assert not res.returncode

        res = subprocess.run(
            ["hg", "log", "--graph", "--template", '"{rev} {desc}"'],
            cwd=cookie.project_dir,
            capture_output=True,
            universal_newlines=True,
        )
        assert not res.returncode
        expected = """
@    "3 Merge in cookiecutter updates"
|\\
| o  "2 BASE: Updated cookiecutter skeleton"
| |
| o  "1 BASE: Initial .cookiecutter.yaml (make hg-init)"
|
o  "0 Initial autocommit of project (make hg-init)"
        """
        assert res.stdout.strip() == expected.strip()

        # Now try changing the project name
        default_context = config["default_context"]
        cookie.update_config(config=config, title=default_context["title"] + " New")
        if default_context["use_course"] == "yes":
            cookie.update_config(config=config, pkg_name="test_project_new")

        res = subprocess.run(
            ["make", "hg-update-cookiecutter"],
            cwd=cookie.project_dir,
            capture_output=True,
            universal_newlines=True,
        )
        assert res.returncode   # Check that this fails...
        assert res.stdout.strip() == "Repo has uncommited changes: Aborting"

        res = subprocess.run(
            ["hg", "commit", "-m", '"Change title"'],
            cwd=cookie.project_dir,
            capture_output=True,
            universal_newlines=True,
        )
        assert not res.returncode

        res = subprocess.run(
            ["make", "hg-update-cookiecutter"],
            cwd=cookie.project_dir,
            capture_output=True,
            universal_newlines=True,
        )
        expected = """
marked working directory as branch cookiecutter-base
removing hg_tmp.*/src/*/__init__.py
removing hg_tmp.*/src/*/example.py
adding hg_tmp.*/src/*test_project_new/__init__.py
adding hg_tmp.*/src/*test_project_new/example.py
removing hg_tmp.*/*test-project
adding hg_tmp.*/*test-project-new
recording removal of hg_tmp.*/src/*/example.py as rename to hg_tmp.*/src/*test_project_new/example.py (100% similar)
recording removal of hg_tmp.*/*test-project as rename to hg_tmp.*/*test-project-new (100% similar)
marked working directory as branch default
About to automerge... if this fails, resolve the issues and then
hg commit -m "Merge in cookiecutter updates"
hg merge cookiecutter-base
* files updated, * files merged, * files removed, * files unresolved
(branch merge, don't forget to commit)
hg commit -m "Merge in cookiecutter updates"
        """
        assert not res.returncode
        assert self.fnmatch(res.stdout, expected)
        return
        breakpoint()

        # Try with HG_UPDATE_ALLOW_DIRTY_REPO=true
        res = subprocess.run(
            ["make", "hg-update-cookiecutter"],
            cwd=cookie.project_dir,
            capture_output=True,
            universal_newlines=True,
            env=os.environ | {"HG_UPDATE_ALLOW_DIRTY_REPO": "true"},
        )
        # Glob-syntax matching
        expected = """
Allowing repo with uncommited changes...
marked working directory as branch cookiecutter-base
removing hg_tmp.*/src/*test_project/__init__.py
removing hg_tmp.*/src/*test_project/example.py
adding hg_tmp.*/src/*test_project_new/__init__.py
adding hg_tmp.*/src/*test_project_new/example.py
removing hg_tmp.*/*test-project
adding hg_tmp.*/*test-project-new
recording removal of hg_tmp.*/src/*test_project/example.py as rename to hg_tmp.*/src/*test_project_new/example.py (100% similar)
recording removal of hg_tmp.*/* as rename to hg_tmp.*/*test-project-new (100% similar)
marked working directory as branch default
About to automerge... if this fails, resolve the issues and then
hg commit -m "Merge in cookiecutter updates"
hg merge cookiecutter-base        
        """
        breakpoint()
        assert not res.returncode
        assert fnmatch(res.stdout.strip(), expected.strip())
